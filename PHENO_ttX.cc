// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/PartonicTops.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedMET.hh"
#include "Rivet/Projections/ParticleFinder.hh"
#include <TTree.h>
#include <TFile.h>
#include <TRandom2.h>
#include <TRandom3.h>
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TMath.h>



using namespace std;
namespace Rivet {


  class NeutrinoWeighter {  

    private:

      ///-- Settings --///
      std::vector< double > m_nu_eta;
      std::vector< double > m_nu_sinh;
      std::vector< double > m_nu_cosh;
      std::vector< double > m_nubar_eta;
      std::vector< double > m_nubar_sinh;
      std::vector< double > m_nubar_cosh;
      std::vector< double > m_top_smears;
      std::vector< double > m_W_smears;
      TRandom3 m_random;
      bool m_do_both_pairings;
      bool m_do_chi2;
      bool m_do_crystalball_smearing;
      bool m_include_x;
      bool m_include_y;
      bool m_include_phi;
      bool m_include_mWp;
      bool m_include_mWn;
      bool m_include_mtop;
      bool m_include_mtbar;

      ///-- Return objects --///
      TLorentzVector m_top;
      TLorentzVector m_tbar;
      TLorentzVector m_ttbar;
      TLorentzVector m_b;
      TLorentzVector m_bbar;
      TLorentzVector m_lep_p;
      TLorentzVector m_lep_m;
      TLorentzVector m_nu;
      TLorentzVector m_nubar;
      double m_weight_max;

    public:

      NeutrinoWeighter(){

        m_top   = TLorentzVector();
        m_tbar  = TLorentzVector();
        m_ttbar = TLorentzVector();
        m_b     = TLorentzVector();
        m_bbar  = TLorentzVector();
        m_lep_p = TLorentzVector();
        m_lep_m = TLorentzVector();
        m_nu    = TLorentzVector();
        m_nubar = TLorentzVector();
        m_random.SetSeed(105200);
        m_weight_max = -99.;
        m_do_both_pairings = false;

        m_include_x     = true;  /// Use MET ex in weight function
        m_include_y     = true;  /// Use MET ey in weight function
        m_include_phi   = false; /// Use MET phi in weight function
        m_include_mWp   = false; /// Use WMass+ smearing in weight function
        m_include_mWn   = false; /// Use WMass- smearing in weight function
        m_include_mtop  = false; /// Use MTop smearing in weight function
        m_include_mtbar = false; /// Use MTbar smearing in weight function

        ///-- Determine eta sampling --///         
        for (double eta = -5.0; eta < 5.0001; eta += 0.2) {
  
          double sinh_eta = sinh(eta);
          double cosh_eta = cosh(eta);

          m_nu_eta.push_back(eta);
          m_nu_sinh.push_back(sinh_eta);
          m_nu_cosh.push_back(cosh_eta);

          m_nubar_eta.push_back(eta);
          m_nubar_sinh.push_back(sinh_eta);
          m_nubar_cosh.push_back(cosh_eta);
        }

        ///-- Do Top Mass Smearing --///
        //m_top_smears.push_back(170.5);
        //m_top_smears.push_back(171.0);
        //m_top_smears.push_back(171.5);
        //m_top_smears.push_back(172.0);
        m_top_smears.push_back(172.5);
        //m_top_smears.push_back(173.0);
        //m_top_smears.push_back(173.5);
        //m_top_smears.push_back(174.0);
        //m_top_smears.push_back(174.5);  

        //m_W_smears.push_back(80.3);
        //m_W_smears.push_back(80.35);
        m_W_smears.push_back(80.4);
        //m_W_smears.push_back(80.45);
        //m_W_smears.push_back(80.5);

      }

      virtual ~NeutrinoWeighter(){};

      TLorentzVector GetTop(){             return m_top;        };
      TLorentzVector GetTbar(){            return m_tbar;       };
      TLorentzVector GetTtbar(){           return m_ttbar;      };
      TLorentzVector GetB(){               return m_b;          };
      TLorentzVector GetBbar(){            return m_bbar;       };
      TLorentzVector GetLpos(){            return m_lep_p;      };
      TLorentzVector GetLneg(){            return m_lep_m;      };      
      TLorentzVector GetNu(){              return m_nu;         };
      TLorentzVector GetNubar(){           return m_nubar;      };
      double GetWeight(){                  return m_weight_max; };

      void RecalculateEtas(double pos_lep_eta, double neg_lep_eta){

        m_nu_eta.clear();
        m_nu_sinh.clear();
        m_nu_cosh.clear();

        m_nubar_eta.clear();
        m_nubar_sinh.clear();
        m_nubar_cosh.clear();

        for (int i = 0; i < 200; ++i){
          double mean_nu = pos_lep_eta*0.7 - 0.1;
          double eta_nu = m_random.Gaus(mean_nu, 1.14);

          m_nu_eta.push_back(eta_nu);
          m_nu_sinh.push_back(sinh(eta_nu));
          m_nu_cosh.push_back(cosh(eta_nu));

          double mean_nubar = neg_lep_eta*0.7 - 0.1;
          double eta_nubar = m_random.Gaus(mean_nubar, 1.14);

          m_nubar_eta.push_back(eta_nubar);
          m_nubar_sinh.push_back(sinh(eta_nubar));
          m_nubar_cosh.push_back(cosh(eta_nubar));
    
        }
      }

      double neutrino_weight(TLorentzVector neutrino1, TLorentzVector neutrino2, double met_ex, double met_ey, double met_phi, TLorentzVector lep_pos, TLorentzVector lep_neg, TLorentzVector b1, TLorentzVector b2, double mtop, double mtbar, double mWp, double mWn){

        double dx = met_ex - neutrino1.Px() - neutrino2.Px();
        double dy = met_ey - neutrino1.Py() - neutrino2.Py();
        double dphi = 1.;
        if(met_phi > -99.){
          TLorentzVector nunubar = neutrino1 + neutrino2;
          dphi = met_phi - nunubar.Phi();
        }
        TLorentzVector Wp   = neutrino1 + lep_pos;
        TLorentzVector Wn   = neutrino2 + lep_neg;
        TLorentzVector top  = neutrino1 + lep_pos + b1;
        TLorentzVector tbar = neutrino2 + lep_neg + b2;

        double dmWp = mWp - Wp.M(); 
        double dmWn = mWn - Wn.M(); 
        double dmtop = mtop - top.M(); 
        double dmtbar = mtbar - tbar.M(); 

        //double m_sigma_met_ex = m_met_sumet*0.023 + 6.5;
        //double m_sigma_met_ey = m_met_sumet*0.023 + 6.5;
        double m_sigma_met_ex  = 0.2*met_ex;
        double m_sigma_met_ey  = 0.2*met_ey;
        double m_sigma_met_phi = 0.05;//0.15
        double m_sigma_mWp     = 0.015;
        double m_sigma_mWn     = 0.015;
        double m_sigma_mtop    = 0.61;
        double m_sigma_mtbar   = 0.61;

        double numerator_x = -dx*dx;
        double numerator_y = -dy*dy;
        double numerator_phi = 1.;
        if(met_phi > -99.)
          numerator_phi = -dphi*dphi;
        double numerator_mWp   = -dmWp*dmWp;
        double numerator_mWn   = -dmWn*dmWn;
        double numerator_mtop  = -dmtop*dmtop;
        double numerator_mtbar = -dmtbar*dmtbar;
  
        double denominator_x = 2.*m_sigma_met_ex*m_sigma_met_ex;
        double denominator_y = 2.*m_sigma_met_ey*m_sigma_met_ey;
        double denominator_phi = 1.;
        if(met_phi > -99.)
          denominator_phi = 2.*m_sigma_met_phi*m_sigma_met_phi;
        double denominator_mWp   = 2.*m_sigma_mWp*m_sigma_mWp;
        double denominator_mWn   = 2.*m_sigma_mWn*m_sigma_mWn;
        double denominator_mtop  = 2.*m_sigma_mtop*m_sigma_mtop;
        double denominator_mtbar = 2.*m_sigma_mtbar*m_sigma_mtbar;
 
        double exp_x = exp(numerator_x/denominator_x);
        double exp_y = exp(numerator_y/denominator_y);
        double exp_phi = 1.;
        if(met_phi > -99.)
          exp_phi = exp(numerator_phi/denominator_phi);
        double exp_mWp   = exp(numerator_mWp/denominator_mWp);
        double exp_mWn   = exp(numerator_mWn/denominator_mWn);
        double exp_mtop  = exp(numerator_mtop/denominator_mtop);
        double exp_mtbar = exp(numerator_mtbar/denominator_mtbar);
  
        double exp_all = 1;
        if(m_include_x){
          exp_all *= exp_x;
        }
        if(m_include_y){
          exp_all *= exp_y;
        }
        if(m_include_phi){
          exp_all *= exp_phi;
        }
        if(m_include_mWp){
          exp_all *= exp_mWp;
        }
        if(m_include_mWn){
          exp_all *= exp_mWn;
        }
        if(m_include_mtop){
          exp_all *= exp_mtop;
        }
        if(m_include_mtbar){
          exp_all *= exp_mtbar;
        }
        
        return exp_all;

      }

      std::vector<TLorentzVector> solveForNeutrinoEta(TLorentzVector* lepton, TLorentzVector* bJet, int index, int index_type, double mtop, double mW) {

        double nu_cosh = -99.;
        double nu_sinh = -99.;

        if (index_type > 0){
          nu_cosh = m_nu_cosh.at(index);
          nu_sinh = m_nu_sinh.at(index);
        } else {
          nu_cosh = m_nubar_cosh.at(index);
          nu_sinh = m_nubar_sinh.at(index);
        }

        //double Wmass2 = 80.4*80.4;
        double Wmass2 = mW*mW;
        double bmass = 4.5;
        double Elprime = lepton->E() * nu_cosh - lepton->Pz() * nu_sinh;
        double Ebprime = bJet->E()   * nu_cosh - bJet->Pz()   * nu_sinh;

        double A = (lepton->Py() * Ebprime - bJet->Py() * Elprime) / (bJet->Px() * Elprime - lepton->Px() * Ebprime);
        double B = (Elprime * (mtop * mtop - Wmass2 - bmass * bmass - 2. * lepton->Dot(*bJet)) - Ebprime * Wmass2) / (2. * (lepton->Px() * Ebprime - bJet->Px() * Elprime));

        double par1 = (lepton->Px() * A + lepton->Py()) / Elprime;
        double C = A * A + 1. - par1 * par1;
        double par2 = (Wmass2 / 2. + lepton->Px() * B) / Elprime;
        double D = 2. * (A * B - par2 * par1);
        double F = B * B - par2 * par2;
        double det = D * D - 4. * C * F;


        std::vector<TLorentzVector> sol;

        ///-- 0 solutions case --///
        if (det < 0.0){
          return sol;                                                                                                                                                 }

        ///-- Only one real solution case --///
        if (det == 0.) {
          double py1 = -D / (2. * C);
          double px1 = A * py1 + B;
          double pT2_1 = px1 * px1 + py1 * py1;
          double pz1 = sqrt(pT2_1) * nu_sinh;

          TLorentzVector a1(px1, py1, pz1, sqrt(pT2_1 + pz1 * pz1));

          if (!TMath::IsNaN(a1.E()) )
            sol.push_back(a1);
          return sol;
        }

        ///-- 2 solutions case --///
        if(det > 0){
          double tmp   = sqrt(det) / (2. * C);
          double py1   = -D / (2. * C) + tmp;
          double py2   = -D / (2. * C) - tmp;
          double px1   = A * py1 + B;
          double px2   = A * py2 + B;
          double pT2_1 = px1 * px1 + py1 * py1;
          double pT2_2 = px2 * px2 + py2 * py2;
          double pz1   = sqrt(pT2_1) * nu_sinh;
          double pz2   = sqrt(pT2_2) * nu_sinh;
          TLorentzVector a1(px1, py1, pz1, sqrt(pT2_1 + pz1 * pz1));
          TLorentzVector a2(px2, py2, pz2, sqrt(pT2_2 + pz2 * pz2));

          if (!TMath::IsNaN(a1.E()) && !TMath::IsNaN(a2.E())){
            sol.push_back(a1);
            sol.push_back(a2);
          }
          return sol;
        }
  
        ///-- Should never reach this point --///
        return sol;
      }


      void calculateWeight(TLorentzVector lepton_pos, TLorentzVector lepton_neg, TLorentzVector b1, TLorentzVector b2, double met_ex, double met_ey, double met_phi, double mtop, double mtbar, double mWp, double mWn) {

        double weight = -1.;
        TLorentzVector top, tbar, ttbar, Wp, Wn;

        for (unsigned int nu_eta_index = 0; nu_eta_index < m_nu_eta.size(); ++nu_eta_index){    

          std::vector<TLorentzVector> solution_1 = solveForNeutrinoEta(&lepton_pos, &b1, nu_eta_index, 1, mtop, mWp);

          if(solution_1.size() == 0){
            continue; // if no solutions in sol1 then continue before calculating sol2;                                                                            
          }
    
          for (unsigned int nubar_eta_index = 0; nubar_eta_index < m_nubar_eta.size(); ++nubar_eta_index){

            std::vector<TLorentzVector> solution_2 = solveForNeutrinoEta(&lepton_neg, &b2, nubar_eta_index, -1, mtbar, mWn);

            if (solution_2.size() == 0){
              continue; // sol2 has no solutions, continue;
            }
      
            ///-- SOLUTION 0, 0 --///
            weight = neutrino_weight(solution_1.at(0), solution_2.at(0), met_ex, met_ey, met_phi, lepton_pos, lepton_neg, b1, b2, mtop, mtbar, mWp, mWn);

            if(weight > m_weight_max && weight > 0.000001){

              top   = (lepton_pos + b1 + solution_1.at(0));
              tbar  = (lepton_neg + b2 + solution_2.at(0));
              ttbar = top + tbar;

              ///-- No point saving non-physical solutons, even if they have high weights --///
              if (ttbar.M() > 300. && top.E() > 0. && tbar.E() > 0.){
                m_weight_max = weight;
                m_top        = top;
                m_tbar       = tbar;
                m_ttbar      = ttbar;
                m_b          = b1;
                m_bbar       = b2;
                m_lep_p      = lepton_pos;
                m_lep_m      = lepton_neg;                
                m_nu         = solution_1.at(0);
                m_nubar      = solution_2.at(0);
              }
            }
      
            ///-- SOLUTION 1, 0 --///
            if (solution_1.size() > 1){

              weight = neutrino_weight(solution_1.at(1), solution_2.at(0), met_ex, met_ey, met_phi, lepton_pos, lepton_neg, b1, b2, mtop, mtbar, mWp, mWn);

              if(weight > m_weight_max && weight > 0.000001){

                top   = (lepton_pos + b1 + solution_1.at(1));
                tbar  = (lepton_neg + b2 + solution_2.at(0));
                ttbar = top + tbar;

                ///-- No point saving non-physical solutons, even if they have high weights --///
                if (ttbar.M() > 300. && top.E() > 0 && tbar.E() > 0){
                  m_weight_max = weight;
                  m_top        = top;
                  m_tbar       = tbar;
                  m_ttbar      = ttbar;
                  m_b          = b1;
                  m_bbar       = b2;
                  m_lep_p      = lepton_pos;
                  m_lep_m      = lepton_neg;  
                  m_nu         = solution_1.at(1);
                  m_nubar      = solution_2.at(0);
                }
              }
            }

            ///-- SOLUTION 0, 1 --///
            if (solution_2.size() > 1){

              weight = neutrino_weight(solution_1.at(0), solution_2.at(1), met_ex, met_ey, met_phi, lepton_pos, lepton_neg, b1, b2, mtop, mtbar, mWp, mWn);

              if(weight > m_weight_max && weight > 0.000001){

                top   = (lepton_pos + b1 + solution_1.at(0));
                tbar  = (lepton_neg + b2 + solution_2.at(1));
                ttbar = top + tbar;

                ///-- No point saving non-physical solutons, even if they have high weights --///
                if (ttbar.M() > 300. && top.E() > 0. && tbar.E() > 0.){
                  m_weight_max = weight;
                  m_top        = top;
                  m_tbar       = tbar;
                  m_ttbar      = ttbar;
                  m_b          = b1;
                  m_bbar       = b2;
                  m_lep_p      = lepton_pos;
                  m_lep_m      = lepton_neg;  
                  m_nu         = solution_1.at(0);
                  m_nubar      = solution_2.at(1);
                }
              }
            }

            ///-- SOLUTION 1, 1 --///
            if (solution_1.size() > 1 && solution_2.size() > 1){

              weight = neutrino_weight(solution_1.at(1), solution_2.at(1), met_ex, met_ey, met_phi, lepton_pos, lepton_neg, b1, b2, mtop, mtbar, mWp, mWn);

              if(weight > m_weight_max && weight > 0.000001){
            
                top   = (lepton_pos + b1 + solution_1.at(1));
                tbar  = (lepton_neg + b2 + solution_2.at(1));
                ttbar = top + tbar;

                ///-- No point saving non-physical solutons, even if they have high weights --///
                if (ttbar.M() > 300. && top.E() > 0 && tbar.E() > 0){
                  m_weight_max = weight;
                  m_top        = top;
                  m_tbar       = tbar;
                  m_ttbar      = ttbar;
                  m_b          = b1;
                  m_bbar       = b2;
                  m_lep_p      = lepton_pos;
                  m_lep_m      = lepton_neg; 
                  m_nu         = solution_1.at(1);
                  m_nubar      = solution_2.at(1);
                }
              }
            }

          }//End of nubar eta index
        }// End of nu eta index
  
        return;
      }

      double GetJetWidth(double jet_pt){
   
        if (jet_pt < 40.0)       return 0.13;
        else if (jet_pt < 50.0)  return 0.14;
        else if (jet_pt < 60.0)  return 0.12;
        else if (jet_pt < 70.0)  return 0.11;
        else if (jet_pt < 80.0)  return 0.09;
        else if (jet_pt < 150.0) return 0.08;
        else                     return 0.06;

      }

      double Reconstruct(std::vector<TLorentzVector> leptons_pos, std::vector<TLorentzVector> leptons_neg, TLorentzVector jet_1, TLorentzVector jet_2, double met_ex, double met_ey, double met_phi){

        ///-- Safety --///
        if(leptons_pos.size() < 1){
          std::cout << "You didn't provide enough positive leptons. You should apply selecton cuts before running NuW!" << std::endl;
          return -1;
        }
        if(leptons_neg.size() < 1){
          std::cout << "You didn't provide enough negative leptons. You should apply selecton cuts before running NuW!" << std::endl;
          return -1;
        }
        if(fabs(met_phi) > 3.15){
          std::cout << "You passed a phi value which didn't make sense, are you sure you're passing the right arguments? " << met_phi << std::endl;
          return -1;
        }        

        std::cout << "Starting Reconstruct " << std::endl;
        std::cout << "Size of lepton_pos: " << leptons_pos.size() << std::endl;       
        std::cout << "Size of lepton_neg: " << leptons_neg.size() << std::endl;                       
        std::cout << "Jet 1: " << jet_1.Pt() << " " << jet_1.Eta() << " " << jet_1.Phi() << " " << jet_1.E() << std::endl;
        std::cout << "Jet 2: " << jet_2.Pt() << " " << jet_2.Eta() << " " << jet_2.Phi() << " " << jet_2.E() << std::endl; 
        std::cout << "Lep p: " << leptons_pos[0].Pt() << " " << leptons_pos[0].Eta() << " " << leptons_pos[0].Phi() << " " << leptons_pos[0].E() << std::endl;                
        std::cout << "Lep m: " << leptons_neg[0].Pt() << " " << leptons_neg[0].Eta() << " " << leptons_neg[0].Phi() << " " << leptons_neg[0].E() << std::endl;                        
        std::cout << "MET:   " << met_ex << " " << met_ey << " " << met_phi << std::endl;
        double width_1 = GetJetWidth(jet_1.Pt());
        double width_2 = GetJetWidth(jet_2.Pt());        
        std::cout << "Got Jet Widths " << std::endl;        

        for(size_t lep_p_index = 0; lep_p_index < leptons_pos.size(); ++lep_p_index){
          TLorentzVector lepton_pos = leptons_pos[lep_p_index];
          for(size_t lep_m_index = 0; lep_m_index < leptons_neg.size(); ++lep_m_index){          
            TLorentzVector lepton_neg = leptons_neg[lep_m_index];
            RecalculateEtas(lepton_pos.Eta(), lepton_neg.Eta()); /// Needs checking if still true for ttX
            for(size_t mtop_counter = 0; mtop_counter < m_top_smears.size(); ++mtop_counter){
              for(size_t mtbar_counter = 0; mtbar_counter < m_top_smears.size(); ++mtbar_counter){
                for(size_t mWp_counter = 0; mWp_counter < m_W_smears.size(); ++mWp_counter){
                  for(size_t mWn_counter = 0; mWn_counter < m_W_smears.size(); ++mWn_counter){

                    double met_ex_original = met_ex;
                    double met_ey_original = met_ey;
    
                    calculateWeight(lepton_pos, lepton_neg, jet_1, jet_2, met_ex, met_ey, met_phi, m_top_smears[mtop_counter], m_top_smears[mtbar_counter], m_W_smears[mWp_counter], m_W_smears[mWn_counter]);
                    if( m_do_both_pairings){
                      calculateWeight(lepton_pos, lepton_neg, jet_2, jet_1, met_ex, met_ey, met_phi, m_top_smears[mtop_counter], m_top_smears[mtbar_counter], m_W_smears[mWp_counter], m_W_smears[mWn_counter]);
                    }
      
                    ///-- Jet Smearing --///
                    for (int smears = 0; smears < 1; ++smears){
                      TLorentzVector jet_1_smeared, jet_2_smeared;
      
                      jet_1_smeared = jet_1;
                      jet_2_smeared = jet_2;
  
                      double scale_1 = 1.;
                      double scale_2 = 2.;
                      scale_1 = m_random.Gaus(jet_1.Pt(), width_1*jet_1.Pt())/jet_1.Pt();
                      scale_2 = m_random.Gaus(jet_2.Pt(), width_2*jet_2.Pt())/jet_2.Pt();
                
                      jet_1_smeared = jet_1_smeared*scale_1;
                      jet_2_smeared = jet_2_smeared*scale_2;
  
                      met_ex = met_ex_original - jet_1.Px() + jet_1_smeared.Px() - jet_2.Px() + jet_2_smeared.Px();
                      met_ey = met_ey_original - jet_1.Py() + jet_1_smeared.Py() - jet_2.Py() + jet_2_smeared.Py();
      
                      if (jet_1_smeared.M() > 0.0 && jet_2_smeared.M() > 0.0){  
                        calculateWeight(lepton_pos, lepton_neg, jet_1_smeared, jet_2_smeared, met_ex, met_ey, met_phi, m_top_smears[mtop_counter], m_top_smears[mtbar_counter], m_W_smears[mWp_counter], m_W_smears[mWn_counter]);
                        if(m_do_both_pairings){
                          calculateWeight(lepton_pos, lepton_neg, jet_2_smeared, jet_1_smeared, met_ex, met_ey, met_phi, m_top_smears[mtop_counter], m_top_smears[mtbar_counter], m_W_smears[mWp_counter], m_W_smears[mWp_counter]);
                        }
                      }
                      met_ex = met_ex_original;
                      met_ey = met_ey_original;
                    } // END OF JET SMEARING 
                  }// END OF Wn MASS SMEARING
                }// END OF Wp MASS SMEARING
              }// END OF TBAR MASS SMEARING
            }// END OF TOP MASS SMEARING
          }//NEGATIVE LEPTON LOOP
        }//POSITIVE LEPTON LOOP

        return m_weight_max;
      }
    };






  /// @brief Add a short analysis description here
  class PHENO_ttX : public Analysis {
    
    public:

      /// Constructor
      DEFAULT_RIVET_ANALYSIS_CTOR(PHENO_ttX);

      /// Book histograms and initialise projections before the run
      void init() {

        random = TRandom2(123456);

        ///////////////////////////////////
        ///-- Final State Projections --///
        ///////////////////////////////////
        double min_eta = -5.;
        double max_eta =  5.;
        double min_pt  =  0.1*GeV;
        FinalState fs_all_objetcs(min_eta, max_eta, min_pt);

        IdentifiedFinalState fs_photons(   fs_all_objetcs);
        IdentifiedFinalState fs_electrons( fs_all_objetcs);
        IdentifiedFinalState fs_muons(     fs_all_objetcs);
        IdentifiedFinalState fs_neutrinos( fs_all_objetcs);

        fs_photons.acceptIdPair(   PID::PHOTON);
        fs_electrons.acceptIdPair( PID::ELECTRON);
        fs_muons.acceptIdPair(     PID::MUON);
        fs_neutrinos.acceptNeutrinos();

        //////////////////////////////////
        ///-- Partonic Level Objects --///
        //////////////////////////////////

        bool emu_from_promt_tau = false;
        declare(PartonicTops(PartonicTops::E_MU, emu_from_promt_tau), "parton_leptonic_tops");
        declare(PartonicTops(PartonicTops::HADRONIC),                 "parton_hadronic_tops");

        //////////////////////////////////
        ///-- Particle Level Objects --///
        //////////////////////////////////

        float dressing_deltaR  = 0.1;

        PromptFinalState particle_electrons(fs_electrons);
        particle_electrons.acceptTauDecays(true); // Probably False?
        addProjection( particle_electrons, "particle_electrons");
        DressedLeptons particle_electrons_dressed( fs_photons, particle_electrons, dressing_deltaR);
        addProjection( particle_electrons_dressed, "particle_electrons_dressed");

        PromptFinalState particle_muons(fs_muons);
        particle_muons.acceptTauDecays(true);
        addProjection( particle_muons, "particle_muons");
        DressedLeptons particle_muons_dressed( fs_photons, particle_muons, dressing_deltaR);
        addProjection( particle_muons_dressed, "particle_muons_dressed");

        PromptFinalState particle_neutrinos(fs_neutrinos);
        particle_neutrinos.acceptTauDecays(true);
        addProjection(particle_neutrinos, "particle_neutrinos");

        VetoedFinalState vfs;
        vfs.addVetoOnThisFinalState(particle_electrons_dressed);
        vfs.addVetoOnThisFinalState(particle_muons_dressed);
        vfs.addVetoOnThisFinalState(particle_neutrinos);
        FastJets particle_jets(vfs, FastJets::ANTIKT, 0.4);
        particle_jets.useInvisibles();
        addProjection(particle_jets, "particle_jets");

        /////////////////////////////////////
        ///-- Reco Level Objects: ATLAS --///
        /////////////////////////////////////

        FastJets reco_jets_atlas(vfs, FastJets::ANTIKT, 0.4);
        MissingMomentum reco_met_atlas(fs_all_objetcs);

        declare(reco_jets_atlas, "reco_jets_atlas");
        declare(reco_met_atlas,  "reco_met_atlas");

        declare(SmearedJets(     reco_jets_atlas,            JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C20),   "reco_jets_atlas_smeared");
        declare(SmearedMET(      reco_met_atlas,             MET_SMEAR_ATLAS_RUN2),                               "reco_met_atlas_smeared");
        declare(SmearedParticles(particle_electrons_dressed, ELECTRON_EFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "reco_electrons_atlas_smeared");
        declare(SmearedParticles(particle_muons_dressed,     MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2),         "reco_muons_atlas_smeared");

        ///////////////////////////////////
        ///-- Reco Level Objects: CMS --///
        ///////////////////////////////////
     
        declare(SmearedJets(     reco_jets_atlas,              JET_SMEAR_CMS_RUN2),                                 "reco_jets_cms_smeared");        
        declare(SmearedMET(      reco_met_atlas,               MET_SMEAR_CMS_RUN2),                                 "reco_met_cms_smeared");
        declare(SmearedParticles(particle_electrons_dressed,   ELECTRON_EFF_CMS_RUN2, ELECTRON_SMEAR_CMS_RUN2),     "reco_electrons_cms_smeared");
        declare(SmearedParticles(particle_muons_dressed,       MUON_EFF_CMS_RUN2, MUON_SMEAR_CMS_RUN2),             "reco_muons_cms_smeared");

        ///////////////////////////////////
        ///--    Output Tree stuff    --///
        ///////////////////////////////////

        output_file          = new TFile("./output_file.root","RECREATE");
        output_tree_reco     = new TTree("reco_level",     "");
        output_tree_particle = new TTree("particle_level", "");
        output_tree_parton   = new TTree("parton_level",   "");

        setBranchesParton(output_tree_parton);
        setBranchesParticle(output_tree_particle);
        setBranchesReco(output_tree_reco);
        
      }

      void analyze(const Event& event) {

        ResetBranches();

        DoPartonLevel(event);

        //DoParticleLevel(event);

        //DoRecoLevel(event);

      }

      void finalize() {
        output_tree_reco->Write();
        output_tree_particle->Write();
        output_tree_parton->Write();                
        output_file->Close();
      }

    private:

      ///-- Particles for projection --///

      TFile* output_file;
      TTree* output_tree_reco;
      TTree* output_tree_particle;
      TTree* output_tree_parton;    
      TRandom2 random;        

      float b_parton_event_weight;
      float b_parton_event_number;
      float b_parton_top_pt;
      float b_parton_top_eta;
      float b_parton_top_phi;
      float b_parton_top_m;
      float b_parton_top_e;
      float b_parton_top_y;
      float b_parton_tbar_pt;
      float b_parton_tbar_eta;
      float b_parton_tbar_phi;
      float b_parton_tbar_m;
      float b_parton_tbar_e;
      float b_parton_tbar_y; 
      float b_parton_ttbar_pt;
      float b_parton_ttbar_eta;
      float b_parton_ttbar_phi;
      float b_parton_ttbar_m;
      float b_parton_ttbar_e;
      float b_parton_ttbar_y; 
      float b_parton_cos_helicity_p;
      float b_parton_cos_helicity_m;
      float b_parton_cos_transverse_p;
      float b_parton_cos_transverse_m;
      float b_parton_cos_raxis_p;
      float b_parton_cos_raxis_m;
      float b_parton_boson_pt;
      float b_parton_boson_eta;
      float b_parton_boson_phi;
      float b_parton_boson_m;
      float b_parton_boson_e;
      float b_parton_boson_y;
      float b_parton_lep_p_pt;  
      float b_parton_lep_p_eta; 
      float b_parton_lep_p_phi; 
      float b_parton_lep_p_e;   
      float b_parton_lep_p_type;         
      float b_parton_lep_m_pt;   
      float b_parton_lep_m_eta;  
      float b_parton_lep_m_phi; 
      float b_parton_lep_m_e;
      float b_parton_lep_m_type;      
      float b_parton_b_pt;          
      float b_parton_b_eta; 
      float b_parton_b_phi; 
      float b_parton_b_e;   
      float b_parton_bbar_pt;   
      float b_parton_bbar_eta;  
      float b_parton_bbar_phi; 
      float b_parton_bbar_e;
      float b_parton_nu_pt;          
      float b_parton_nu_eta; 
      float b_parton_nu_phi; 
      float b_parton_nu_e;   
      float b_parton_nubar_pt;   
      float b_parton_nubar_eta;  
      float b_parton_nubar_phi; 
      float b_parton_nubar_e;
      float b_parton_met_et;
      float b_parton_met_phi;
      float b_parton_met_ex;
      float b_parton_met_ey;
      int b_particle_nus_n;

      float b_particle_event_weight;
      float b_particle_event_number;
      float b_particle_top_pt;   
      float b_particle_top_eta;  
      float b_particle_top_phi; 
      float b_particle_top_m; 
      float b_particle_top_e;   
      float b_particle_top_y;                               
      float b_particle_tbar_pt;  
      float b_particle_tbar_eta; 
      float b_particle_tbar_phi; 
      float b_particle_tbar_m;   
      float b_particle_tbar_e;   
      float b_particle_tbar_y;    
      float b_particle_ttbar_pt; 
      float b_particle_ttbar_eta;
      float b_particle_ttbar_phi;
      float b_particle_ttbar_m;  
      float b_particle_ttbar_e;  
      float b_particle_ttbar_y;
      float b_particle_cos_helicity_p;
      float b_particle_cos_helicity_m;
      float b_particle_cos_transverse_p;
      float b_particle_cos_transverse_m;
      float b_particle_cos_raxis_p;
      float b_particle_cos_raxis_m;
      float b_particle_boson_pt;
      float b_particle_boson_eta;
      float b_particle_boson_phi;
      float b_particle_boson_m;
      float b_particle_boson_e;
      float b_particle_boson_y;
      float b_particle_lep_p_pt;  
      float b_particle_lep_p_eta; 
      float b_particle_lep_p_phi; 
      float b_particle_lep_p_e;   
      float b_particle_lep_p_type;         
      float b_particle_lep_m_pt;   
      float b_particle_lep_m_eta;  
      float b_particle_lep_m_phi; 
      float b_particle_lep_m_e;
      float b_particle_lep_m_type;      
      float b_particle_b_pt;          
      float b_particle_b_eta; 
      float b_particle_b_phi; 
      float b_particle_b_e;   
      float b_particle_bbar_pt;   
      float b_particle_bbar_eta;  
      float b_particle_bbar_phi; 
      float b_particle_bbar_e;
      float b_particle_nu_pt;          
      float b_particle_nu_eta; 
      float b_particle_nu_phi; 
      float b_particle_nu_e;   
      float b_particle_nubar_pt;   
      float b_particle_nubar_eta;  
      float b_particle_nubar_phi; 
      float b_particle_nubar_e;
      float b_particle_met_et;
      float b_marticle_met_phi;
      float b_particle_met_ex;
      float b_particle_met_ey; 
      int b_particle_lep_n;      
      std::vector<float> b_particle_lep_pt;   
      std::vector<float> b_particle_lep_eta;  
      std::vector<float> b_particle_lep_phi; 
      std::vector<float> b_particle_lep_e;  
      std::vector<float> b_particle_lep_charge;
      std::vector<float> b_particle_lep_type;      
      int b_particle_jet_n;
      std::vector<float> b_particle_jet_pt;   
      std::vector<float> b_particle_jet_eta;  
      std::vector<float> b_particle_jet_phi; 
      std::vector<float> b_particle_jet_e;  
      std::vector<float> b_particle_jet_charge;
      int b_particle_bjet_n;
      std::vector<float> b_particle_bjet_pt;   
      std::vector<float> b_particle_bjet_eta;  
      std::vector<float> b_particle_bjet_phi; 
      std::vector<float> b_particle_bjet_e;  
      std::vector<float> b_particle_bjet_charge;

      float b_reco_event_weight;
      float b_reco_event_number;

      float b_atlas_top_pt;   
      float b_atlas_top_eta;  
      float b_atlas_top_phi; 
      float b_atlas_top_m; 
      float b_atlas_top_e;   
      float b_atlas_top_y;                               
      float b_atlas_tbar_pt;  
      float b_atlas_tbar_eta; 
      float b_atlas_tbar_phi; 
      float b_atlas_tbar_m;   
      float b_atlas_tbar_e;   
      float b_atlas_tbar_y;    
      float b_atlas_ttbar_pt; 
      float b_atlas_ttbar_eta;
      float b_atlas_ttbar_phi;
      float b_atlas_ttbar_m;  
      float b_atlas_ttbar_e;  
      float b_atlas_ttbar_y;
      float b_atlas_cos_helicity_p;
      float b_atlas_cos_helicity_m;
      float b_atlas_cos_transverse_p;
      float b_atlas_cos_transverse_m;
      float b_atlas_cos_raxis_p;
      float b_atlas_cos_raxis_m;
      float b_atlas_boson_pt;
      float b_atlas_boson_eta;
      float b_atlas_boson_phi;
      float b_atlas_boson_m;
      float b_atlas_boson_e;
      float b_atlas_boson_y;
      float b_atlas_lep_p_pt;  
      float b_atlas_lep_p_eta; 
      float b_atlas_lep_p_phi; 
      float b_atlas_lep_p_e;   
      float b_atlas_lep_p_type;         
      float b_atlas_lep_m_pt;   
      float b_atlas_lep_m_eta;  
      float b_atlas_lep_m_phi; 
      float b_atlas_lep_m_e;
      float b_atlas_lep_m_type;      
      float b_atlas_b_pt;          
      float b_atlas_b_eta; 
      float b_atlas_b_phi; 
      float b_atlas_b_e;   
      float b_atlas_bbar_pt;   
      float b_atlas_bbar_eta;  
      float b_atlas_bbar_phi; 
      float b_atlas_bbar_e;
      float b_atlas_nu_pt;          
      float b_atlas_nu_eta; 
      float b_atlas_nu_phi; 
      float b_atlas_nu_e;   
      float b_atlas_nubar_pt;   
      float b_atlas_nubar_eta;  
      float b_atlas_nubar_phi; 
      float b_atlas_nubar_e;
      float b_atlas_met_et;
      float b_atlas_met_phi;
      float b_atlas_met_ex;
      float b_atlas_met_ey; 
      int b_atlas_lep_n;      
      std::vector<float> b_atlas_lep_pt;   
      std::vector<float> b_atlas_lep_eta;  
      std::vector<float> b_atlas_lep_phi; 
      std::vector<float> b_atlas_lep_e;  
      std::vector<float> b_atlas_lep_charge;
      std::vector<float> b_atlas_lep_type;      
      int b_atlas_jet_n;
      std::vector<float> b_atlas_jet_pt;   
      std::vector<float> b_atlas_jet_eta;  
      std::vector<float> b_atlas_jet_phi; 
      std::vector<float> b_atlas_jet_e;  
      int b_atlas_bjet_n;
      std::vector<float> b_atlas_bjet_pt;   
      std::vector<float> b_atlas_bjet_eta;  
      std::vector<float> b_atlas_bjet_phi; 
      std::vector<float> b_atlas_bjet_e;  

      float b_cms_top_pt;   
      float b_cms_top_eta;  
      float b_cms_top_phi; 
      float b_cms_top_m; 
      float b_cms_top_e;   
      float b_cms_top_y;                               
      float b_cms_tbar_pt;  
      float b_cms_tbar_eta; 
      float b_cms_tbar_phi; 
      float b_cms_tbar_m;   
      float b_cms_tbar_e;   
      float b_cms_tbar_y;    
      float b_cms_ttbar_pt; 
      float b_cms_ttbar_eta;
      float b_cms_ttbar_phi;
      float b_cms_ttbar_m;  
      float b_cms_ttbar_e;  
      float b_cms_ttbar_y;
      float b_cms_cos_helicity_p;
      float b_cms_cos_helicity_m;
      float b_cms_cos_transverse_p;
      float b_cms_cos_transverse_m;
      float b_cms_cos_raxis_p;
      float b_cms_cos_raxis_m;
      float b_cms_boson_pt;
      float b_cms_boson_eta;
      float b_cms_boson_phi;
      float b_cms_boson_m;
      float b_cms_boson_e;
      float b_cms_boson_y;
      float b_cms_lep_p_pt;  
      float b_cms_lep_p_eta; 
      float b_cms_lep_p_phi; 
      float b_cms_lep_p_e;   
      float b_cms_lep_p_type;         
      float b_cms_lep_m_pt;   
      float b_cms_lep_m_eta;  
      float b_cms_lep_m_phi; 
      float b_cms_lep_m_e;
      float b_cms_lep_m_type;      
      float b_cms_b_pt;          
      float b_cms_b_eta; 
      float b_cms_b_phi; 
      float b_cms_b_e;   
      float b_cms_bbar_pt;   
      float b_cms_bbar_eta;  
      float b_cms_bbar_phi; 
      float b_cms_bbar_e;
      float b_cms_nu_pt;          
      float b_cms_nu_eta; 
      float b_cms_nu_phi; 
      float b_cms_nu_e;   
      float b_cms_nubar_pt;   
      float b_cms_nubar_eta;  
      float b_cms_nubar_phi; 
      float b_cms_nubar_e;
      float b_cms_met_et;
      float b_cms_met_phi;
      float b_cms_met_ex;
      float b_cms_met_ey; 
      int b_cms_lep_n;      
      std::vector<float> b_cms_lep_pt;   
      std::vector<float> b_cms_lep_eta;  
      std::vector<float> b_cms_lep_phi; 
      std::vector<float> b_cms_lep_e;  
      std::vector<float> b_cms_lep_charge;
      std::vector<float> b_cms_lep_type;      
      int b_cms_jet_n;
      std::vector<float> b_cms_jet_pt;   
      std::vector<float> b_cms_jet_eta;  
      std::vector<float> b_cms_jet_phi; 
      std::vector<float> b_cms_jet_e;  
      int b_cms_bjet_n;
      std::vector<float> b_cms_bjet_pt;   
      std::vector<float> b_cms_bjet_eta;  
      std::vector<float> b_cms_bjet_phi; 
      std::vector<float> b_cms_bjet_e;  

      ///////////////////////////////////////////////////////////////////////////
      ///--                          PARTON LEVEL                           --///
      ///////////////////////////////////////////////////////////////////////////

      void DoPartonLevel(const Event& event){

        const double   weight = event.weight();
        b_parton_event_weight = weight;

        //Ensures same event weight//
        int event_number = random.Integer(999999999);
        b_parton_event_number   = event_number;
        b_particle_event_number = event_number;
        b_reco_event_number     = event_number;

        const Particles leptonicpartontops = apply<ParticleFinder>(event, "parton_leptonic_tops").particlesByPt();
        const Particles hadronicpartontops = apply<ParticleFinder>(event, "parton_hadronic_tops").particlesByPt();

        const bool isSemilepton = (leptonicpartontops.size() == 1 && hadronicpartontops.size() == 1);
        const bool isDilepton   = (leptonicpartontops.size() == 2 && hadronicpartontops.size() == 0); 

        if (!isSemilepton && !isDilepton){
          return;
        }
        
        const Particle top1          = leptonicpartontops[0];
        const Particle top2          = leptonicpartontops[1];
        const Particle leadingTop    = top1.momentum().pT() > top2.momentum().pT() ? top1 : top2;
        const Particle subleadingTop = top1.momentum().pT() < top2.momentum().pT() ? top1 : top2;
        const Particle top           = top1.pid() ==  6 ? top1 : top2;      
        const Particle tbar          = top2.pid() == -6 ? top2 : top1;        

        //See if we can find a H or Z boson

        Particles Zbosons = filter_select(event.allParticles(), lastParticleWith(isZ));
        Particles Hbosons = filter_select(event.allParticles(), lastParticleWith(isHiggs));

        if(Zbosons.size() !=2 && Zbosons.size() > 0 && Hbosons.size() > 0){
          MSG_WARNING("Something looks weird with the number of Higgs and Z bosons");
          return;
        }

        if(Hbosons.size() == 1){
          TLorentzVector boson = ConvertParticle(Hbosons[0]);
          b_parton_boson_pt  = boson.Pt();
          b_parton_boson_eta = boson.Eta();
          b_parton_boson_phi = boson.Phi();
          b_parton_boson_m   = boson.M();
          b_parton_boson_e   = boson.E();
          b_parton_boson_y   = boson.Rapidity();
        } else if (Zbosons.size() == 1){
          TLorentzVector boson = ConvertParticle(Zbosons[0]);
          b_parton_boson_pt  = boson.Pt();
          b_parton_boson_eta = boson.Eta();
          b_parton_boson_phi = boson.Phi();
          b_parton_boson_m   = boson.M();
          b_parton_boson_e   = boson.E();
          b_parton_boson_y   = boson.Rapidity();       
        }

        if(isDilepton){

          const auto isPromptChargedLepton = [](const Particle& p){return (isChargedLepton(p) && isPrompt(p, false, false));};
          const auto isPromptNeutrino      = [](const Particle& p){return (isNeutrino(p)      && isPrompt(p, false, false));};
          const auto isPromptBottom        = [](const Particle& p){return (isBottom(p)        && isPrompt(p, false, false));};          

          Particles candidates_lep_p = top.allDescendants(firstParticleWith(isPromptChargedLepton),  false);
          Particles candidates_b     = top.allDescendants(firstParticleWith(isPromptBottom),         false);
          Particles candidates_nu    = top.allDescendants(firstParticleWith(isPromptNeutrino),       false);

          Particles candidates_lep_m = tbar.allDescendants(firstParticleWith(isPromptChargedLepton), false);
          Particles candidates_bbar  = tbar.allDescendants(firstParticleWith(isPromptBottom),        false);
          Particles candidates_nubar = tbar.allDescendants(firstParticleWith(isPromptNeutrino),      false);         

          if (candidates_lep_p.size() > 1){
            MSG_WARNING("Multiple candidates for lep_p, taking the first one");
            return;
          }
          if (candidates_lep_m.size() > 1){
            MSG_WARNING("Multiple candidates for lep_m, taking the first one");
            return;
          }
          if (candidates_nu.size() > 1){
            MSG_WARNING("Multiple candidates for nu, taking the first one");
            return;
          }
          if (candidates_nubar.size() > 1){
            MSG_WARNING("Multiple candidates for nubar, taking the first one");
            return;
          }
          if (candidates_b.size() > 1){
            MSG_WARNING("Multiple candidates for b, taking the first one");
            return;
          }
          if (candidates_bbar.size() > 1){
            MSG_WARNING("Multiple candidates for bbar, taking the first one");
            return;
          }

          Particle lep_p = candidates_lep_p[0];
          Particle lep_m = candidates_lep_m[0];   
          Particle nu    = candidates_nu[0];
          Particle nubar = candidates_nubar[0];
          Particle b     = candidates_b[0];      
          Particle bbar  = candidates_bbar[0];              

          if(lep_p.charge()*lep_m.charge() >= 0){
            MSG_WARNING("same sign top leptons, or charge problem, skipping");
            return;
          }

          ///-- Deal with inconsistent phi conventions and other weirdness --///
          TLorentzVector v_top   = ConvertParticle(top);
          TLorentzVector v_tbar  = ConvertParticle(tbar);  
          TLorentzVector v_lep_p = ConvertParticle(lep_p); 
          TLorentzVector v_lep_m = ConvertParticle(lep_m); 
          TLorentzVector v_b     = ConvertParticle(b);    
          TLorentzVector v_bbar  = ConvertParticle(bbar);              
          TLorentzVector v_nu    = ConvertParticle(nu);    
          TLorentzVector v_nubar = ConvertParticle(nubar);
          TLorentzVector v_ttbar = v_top + v_tbar;
          TLorentzVector v_met   = v_nu + v_nubar;

          b_parton_top_pt            = v_top.Pt();  
          b_parton_top_eta           = v_top.Eta();   
          b_parton_top_phi           = v_top.Phi();   
          b_parton_top_m             = v_top.M(); 
          b_parton_top_e             = v_top.E(); 
          b_parton_top_y             = v_top.Rapidity(); 
          b_parton_tbar_pt           = v_tbar.Pt();   
          b_parton_tbar_eta          = v_tbar.Eta();    
          b_parton_tbar_phi          = v_tbar.Phi();    
          b_parton_tbar_m            = v_tbar.M();  
          b_parton_tbar_e            = v_tbar.E();  
          b_parton_tbar_y            = v_tbar.Rapidity();  
          b_parton_ttbar_pt          = v_ttbar.Pt();    
          b_parton_ttbar_eta         = v_ttbar.Eta();     
          b_parton_ttbar_phi         = v_ttbar.Phi();     
          b_parton_ttbar_m           = v_ttbar.M();   
          b_parton_ttbar_e           = v_ttbar.E();   
          b_parton_ttbar_y           = v_ttbar.Rapidity(); 
          b_parton_lep_p_pt          = v_lep_p.Pt();  
          b_parton_lep_p_eta         = v_lep_p.Eta(); 
          b_parton_lep_p_phi         = v_lep_p.Phi(); 
          b_parton_lep_p_e           = v_lep_p.E();   
          b_parton_lep_p_type        = lep_p.pid();         
          b_parton_lep_m_pt          = v_lep_m.Pt();   
          b_parton_lep_m_eta         = v_lep_m.Eta();  
          b_parton_lep_m_phi         = v_lep_m.Phi(); 
          b_parton_lep_m_e           = v_lep_m.E();
          b_parton_lep_m_type        = lep_m.pid(); 
          b_parton_b_pt              = v_b.Pt();           
          b_parton_b_eta             = v_b.Eta(); 
          b_parton_b_phi             = v_b.Phi(); 
          b_parton_b_e               = v_b.E();   
          b_parton_bbar_pt           = v_bbar.Pt();           
          b_parton_bbar_eta          = v_bbar.Eta(); 
          b_parton_bbar_phi          = v_bbar.Phi(); 
          b_parton_bbar_e            = v_bbar.E(); 
          b_parton_nu_pt             = v_nu.Pt();           
          b_parton_nu_eta            = v_nu.Eta(); 
          b_parton_nu_phi            = v_nu.Phi(); 
          b_parton_nu_e              = v_nu.E();   
          b_parton_nubar_pt          = v_nubar.Pt();           
          b_parton_nubar_eta         = v_nubar.Eta(); 
          b_parton_nubar_phi         = v_nubar.Phi(); 
          b_parton_nubar_e           = v_nubar.E(); 
          b_parton_met_et            = v_met.Pt();
          b_parton_met_phi           = v_met.Phi();
          b_parton_met_ex            = v_met.Px();
          b_parton_met_ey            = v_met.Py();
          b_parton_cos_helicity_p    = cos_theta_helicity(   v_top, v_top,  v_ttbar, v_lep_p, +1);
          b_parton_cos_helicity_m    = cos_theta_helicity(   v_top, v_tbar, v_ttbar, v_lep_m, -1);
          b_parton_cos_transverse_p  = cos_theta_transverse( v_top, v_top,  v_ttbar, v_lep_p, +1);
          b_parton_cos_transverse_m  = cos_theta_transverse( v_top, v_tbar, v_ttbar, v_lep_m, -1);
          b_parton_cos_raxis_p       = cos_theta_raxis(      v_top, v_top,  v_ttbar, v_lep_p, +1);
          b_parton_cos_raxis_m       = cos_theta_raxis(      v_top, v_tbar, v_ttbar, v_lep_m, -1);

        }

        output_tree_parton->Fill();
      }

      ///////////////////////////////////////////////////////////////////////////
      ///--                        PARTICLE LEVEL                           --///
      ///////////////////////////////////////////////////////////////////////////

      void DoParticleLevel(const Event& event){

        ///-- Get Event Weight --///
        b_particle_event_weight = event.weight();

        ///-- Get the selected objects, using the projections --///
        vector<DressedLepton> electrons_dressed  = sortByPt(applyProjection<DressedLeptons>(   event, "particle_electrons_dressed").dressedLeptons());
        vector<DressedLepton> muons_dressed      = sortByPt(applyProjection<DressedLeptons>(   event, "particle_muons_dressed").dressedLeptons());
        Particles particle_neutrinos             = sortByPt(applyProjection<PromptFinalState>( event, "particle_neutrinos").particlesByPt());
        Jets particle_jets                       = sortByPt(applyProjection<FastJets>(         event, "particle_jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5));

        ///-- apply quality selections --///
        vector<DressedLepton> particle_electrons = SelectParticleLeptons(electrons_dressed);
        vector<DressedLepton> particle_muons     = SelectParticleLeptons(muons_dressed);
        Jets particle_ljets                      = SelectParticleLightJets(particle_jets);
        Jets particle_bjets                      = SelectParticleBJets(particle_jets);
        vector<DressedLepton> particle_leptons   = particle_electrons + particle_muons;

        if(particle_bjets.size() < 1){
          return;
        }

        ///-- Fill basic branches --///    
        b_particle_lep_n = particle_leptons.size();        
        foreach(DressedLepton lep, particle_leptons){ 
          TLorentzVector temp = ConvertParticle(lep.momentum());
          b_particle_lep_pt.push_back(     temp.Pt());
          b_particle_lep_eta.push_back(    temp.Eta());
          b_particle_lep_phi.push_back(    temp.Phi());
          b_particle_lep_e.push_back(      temp.E());
          b_particle_lep_charge.push_back( lep.charge());
          b_particle_lep_type.push_back(   lep.pid());
        }

        vector<TLorentzVector> ljets;
        b_particle_jet_n = particle_ljets.size();
        foreach(Jet jet, particle_ljets){
          TLorentzVector temp = ConvertParticle(jet.momentum());
          b_particle_jet_pt.push_back( temp.Pt());
          b_particle_jet_eta.push_back(temp.Eta());
          b_particle_jet_phi.push_back(temp.Phi());
          b_particle_jet_e.push_back(  temp.E());
          ljets.push_back(temp);
        }

        vector<TLorentzVector> bjets;
        b_particle_bjet_n = particle_bjets.size();
        foreach(Jet jet, particle_bjets){
          TLorentzVector temp = ConvertParticle(jet.momentum());                    
          b_particle_bjet_pt.push_back( temp.Pt());
          b_particle_bjet_eta.push_back(temp.Eta());
          b_particle_bjet_phi.push_back(temp.Phi());
          b_particle_bjet_e.push_back(  temp.E());
          bjets.push_back(temp);
        }

        vector<TLorentzVector> neutrinos;
        TLorentzVector met;
        b_particle_nus_n = particle_neutrinos.size();
        foreach(Particle nu, particle_neutrinos){
          TLorentzVector temp = ConvertParticle(nu);
          met = met + temp;
          neutrinos.push_back(temp);
        }

        b_particle_met_et  = met.Pt();
        b_marticle_met_phi = met.Phi();
        b_particle_met_ex  = met.Px();
        b_particle_met_ey  = met.Py();


        //// Jay you're here, you're in the middle of changing particle types from FourMomentum to TLorentzVectors.
        //// Next job is to change leptons, ljets, and bjets to be TLorentzVectors and to remove duplicate code for sorting pos and neg leps.
        ///-- Now do top reconstruction --///
        /// Maybe include netrino numbers here?? ///
        if(b_particle_lep_n == 2){
          ParticleReconstructionDilepton(   particle_electrons, particle_muons, ljets, bjets, neutrinos);
        } else if (b_particle_lep_n == 3){
          ParticleReconstructionTrilepton(  particle_electrons, particle_muons, ljets, bjets, neutrinos);
        } else if (b_particle_lep_n == 4){
          ParticleReconstructionTetralepton(particle_electrons, particle_muons, ljets, bjets, neutrinos);
        } else {
          return;
        }

        output_tree_particle->Fill();
      }

      ///////////////////////////////////////////////////////////////////////////
      ///--                          RECO LEVEL                             --///
      ///////////////////////////////////////////////////////////////////////////

      void DoRecoLevel(const Event& event){

        ///-- Get Event Weight --///
        const double weight = event.weight();
        b_reco_event_weight = weight;

        ///-- Get the selected objects, using the projections --///
        Particles m_electrons_atlas = sortByPt(applyProjection<SmearedParticles>( event, "reco_electrons_atlas_smeared").particles());
        Particles m_muons_atlas     = sortByPt(applyProjection<SmearedParticles>( event, "reco_muons_atlas_smeared").particles());
        Vector3 m_met_atlas         = applyProjection<SmearedMET>(                event, "reco_met_atlas_smeared").vectorMET();
        double m_met_atlas_met      = applyProjection<SmearedMET>(                event, "reco_met_atlas_smeared").met();        
        Jets m_jets_atlas           = sortByPt(applyProjection<SmearedJets>(      event, "reco_jets_atlas_smeared").jetsByPt(Cuts::pT > 25*GeV));

        Particles m_electrons_cms   = sortByPt(applyProjection<SmearedParticles>( event, "reco_electrons_cms_smeared").particles());
        Particles m_muons_cms       = sortByPt(applyProjection<SmearedParticles>( event, "reco_muons_cms_smeared").particles());
        Vector3 m_met_cms           = applyProjection<SmearedMET>(                event, "reco_met_cms_smeared").vectorMET();        
        double m_met_cms_met        = applyProjection<SmearedMET>(                event, "reco_met_cms_smeared").met();
        Jets m_jets_cms             = sortByPt(applyProjection<SmearedJets>(      event, "reco_jets_cms_smeared").jetsByPt(Cuts::pT > 25*GeV));

        std::vector<TLorentzVector> atlas_good_electrons;
        std::vector<TLorentzVector> atlas_good_muons;
        std::vector<int>            atlas_good_electrons_charge;
        std::vector<int>            atlas_good_muons_charge;
        std::vector<TLorentzVector> atlas_good_jets;
        std::vector<TLorentzVector> atlas_good_bjets;   

        std::vector<TLorentzVector> cms_good_electrons;
        std::vector<TLorentzVector> cms_good_muons;
        std::vector<int>            cms_good_electrons_charge;
        std::vector<int>            cms_good_muons_charge;
        std::vector<TLorentzVector> cms_good_jets;
        std::vector<TLorentzVector> cms_good_bjets;  

        /////////////////////////////////////
        ////          ATLAS RECO         ////
        /////////////////////////////////////

        foreach(Particle el, m_electrons_atlas){ 
          if(el.momentum().pT() < 25.){
            continue;
          }
          if(fabs(el.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(el.momentum().pT(), el.momentum().eta(), el.momentum().phi(), el.momentum().E());
          atlas_good_electrons.push_back(temp);
          if(el.pdgId() > 0){
            atlas_good_electrons_charge.push_back(-1);
          } else {
            atlas_good_electrons_charge.push_back(+1);
          }
        }

        foreach(Particle mu, m_muons_atlas){ 
          if(mu.momentum().pT() < 25.){
            continue;
          }
          if(fabs(mu.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(mu.momentum().pT(), mu.momentum().eta(), mu.momentum().phi(), mu.momentum().E());
          atlas_good_muons.push_back(temp);
          if(mu.pdgId() > 0){
            atlas_good_muons_charge.push_back(-1);
          } else {
            atlas_good_muons_charge.push_back(+1);
          }
        }

        foreach(Jet jet, m_jets_atlas){ 
          if(jet.momentum().pT() < 25.){
            continue;
          }
          if(fabs(jet.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(jet.momentum().pT(), jet.momentum().eta(), jet.momentum().phi(), jet.momentum().E());

          if(jet.bTagged()){
            atlas_good_bjets.push_back(temp);
          } else {
            atlas_good_jets.push_back(temp);
          }
        }

        ///-- Overlap Removal --///

        /// Remove any jets that overlap with electrons
        for( uint i = 0; i < atlas_good_electrons.size(); ++i){
          for( uint j = 0; j < atlas_good_jets.size(); ++j){
            if(atlas_good_electrons[i].DeltaR(atlas_good_jets[j]) < 0.4){
              atlas_good_jets.erase(atlas_good_jets.begin() + j);
              --j;
            }
          }
        }

        /// Remove any muons that overlap with jets
        for( uint i = 0; i < atlas_good_jets.size(); ++i){
          for( uint j = 0; j < atlas_good_muons.size(); ++j){
            if(atlas_good_muons[j].DeltaR(atlas_good_jets[i]) < 0.4){
              atlas_good_muons.erase(atlas_good_muons.begin() + j);
              atlas_good_muons_charge.erase(atlas_good_muons_charge.begin() + j);              
              --j;
            }
          }
        }

        b_atlas_lep_n = atlas_good_electrons.size() + atlas_good_muons.size();
        std::vector<TLorentzVector> atlas_leptons_pos; /// For NuW
        std::vector<TLorentzVector> atlas_leptons_neg; /// For NuW
        for(uint i = 0; i < atlas_good_electrons.size(); ++i){
          TLorentzVector temp;
          temp.SetPtEtaPhiE(atlas_good_electrons[i].Pt(), atlas_good_electrons[i].Eta(), atlas_good_electrons[i].Phi(), atlas_good_electrons[i].E());
          b_atlas_lep_pt.push_back(     atlas_good_electrons[i].Pt());   
          b_atlas_lep_eta.push_back(    atlas_good_electrons[i].Eta());  
          b_atlas_lep_phi.push_back(    atlas_good_electrons[i].Phi()); 
          b_atlas_lep_e.push_back(      atlas_good_electrons[i].E());  
          b_atlas_lep_charge.push_back( atlas_good_electrons_charge[i]);
          b_atlas_lep_type.push_back(11*atlas_good_electrons_charge[i]);
          if(atlas_good_electrons_charge[i] > 0){
            atlas_leptons_pos.push_back(temp);
          } else {
            atlas_leptons_neg.push_back(temp);
          }
        }
        for(uint i = 0; i < atlas_good_muons.size(); ++i){
          TLorentzVector temp;
          temp.SetPtEtaPhiE(atlas_good_muons[i].Pt(), atlas_good_muons[i].Eta(), atlas_good_muons[i].Phi(), atlas_good_muons[i].E());          
          b_atlas_lep_pt.push_back(     atlas_good_muons[i].Pt());   
          b_atlas_lep_eta.push_back(    atlas_good_muons[i].Eta());  
          b_atlas_lep_phi.push_back(    atlas_good_muons[i].Phi()); 
          b_atlas_lep_e.push_back(      atlas_good_muons[i].E());  
          b_atlas_lep_charge.push_back( atlas_good_muons_charge[i]);
          b_atlas_lep_type.push_back(13*atlas_good_muons_charge[i]);
          if(atlas_good_muons_charge[i] > 0){
            atlas_leptons_pos.push_back(temp);
          } else {
            atlas_leptons_neg.push_back(temp);
          }           
        }   
        for(uint i = 0; i < atlas_good_jets.size(); ++i){
          b_atlas_jet_pt.push_back(     atlas_good_jets[i].Pt());   
          b_atlas_jet_eta.push_back(    atlas_good_jets[i].Eta());  
          b_atlas_jet_phi.push_back(    atlas_good_jets[i].Phi()); 
          b_atlas_jet_e.push_back(      atlas_good_jets[i].E());  
        } 
        std::vector<TLorentzVector> atlas_bjets;
        for(uint i = 0; i < atlas_good_bjets.size(); ++i){
          TLorentzVector temp;
          temp.SetPtEtaPhiE(atlas_good_bjets[i].Pt(), atlas_good_bjets[i].Eta(), atlas_good_bjets[i].Phi(), atlas_good_bjets[i].E());
          b_atlas_bjet_pt.push_back(    atlas_good_bjets[i].Pt());   
          b_atlas_bjet_eta.push_back(   atlas_good_bjets[i].Eta());  
          b_atlas_bjet_phi.push_back(   atlas_good_bjets[i].Phi()); 
          b_atlas_bjet_e.push_back(     atlas_good_bjets[i].E()); 
          atlas_bjets.push_back(temp); 
        } 

        b_atlas_met_et   = m_met_atlas_met;
        b_atlas_met_phi  = m_met_atlas.phi() - TMath::Pi();
        b_atlas_met_ex   = TMath::Cos(b_atlas_met_phi)*m_met_atlas_met;
        b_atlas_met_ey   = TMath::Sin(b_atlas_met_phi)*m_met_atlas_met;


        /////////////////////////////////////
        ////           CMS RECO          ////
        /////////////////////////////////////


        foreach(Particle el, m_electrons_cms){ 
          if(el.momentum().pT() < 25.){
            continue;
          }
          if(fabs(el.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(el.momentum().pT(), el.momentum().eta(), el.momentum().phi(), el.momentum().E());
          cms_good_electrons.push_back(temp);
          if(el.pdgId() > 0){
            cms_good_electrons_charge.push_back(-1);
          } else {
            cms_good_electrons_charge.push_back(+1);
          }
        }

        foreach(Particle mu, m_muons_cms){ 
          if(mu.momentum().pT() < 25.){
            continue;
          }
          if(fabs(mu.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(mu.momentum().pT(), mu.momentum().eta(), mu.momentum().phi(), mu.momentum().E());
          cms_good_muons.push_back(temp);
          if(mu.pdgId() > 0){
            cms_good_muons_charge.push_back(-1);
          } else {
            cms_good_muons_charge.push_back(+1);
          }
        }

        foreach(Jet jet, m_jets_cms){ 
          if(jet.momentum().pT() < 25.){
            continue;
          }
          if(fabs(jet.momentum().eta()) > 2.5){
            continue;
          }
          TLorentzVector temp;
          temp.SetPtEtaPhiE(jet.momentum().pT(), jet.momentum().eta(), jet.momentum().phi(), jet.momentum().E());

          if(jet.bTagged()){
            cms_good_bjets.push_back(temp);
          } else {
            cms_good_jets.push_back(temp);
          }
        }
        
        ///-- Overlap Removal --///

        /// Remove any jets that overlap with electrons
        for( uint i = 0; i < cms_good_electrons.size(); ++i){
          for( uint j = 0; j < cms_good_jets.size(); ++j){
            if(cms_good_electrons[i].DeltaR(cms_good_jets[j]) < 0.4){
              cms_good_jets.erase(cms_good_jets.begin() + j);
              --j;
            }
          }
        }

        /// Remove any muons that overlap with jets
        for( uint i = 0; i < cms_good_jets.size(); ++i){
          for( uint j = 0; j < cms_good_muons.size(); ++j){
            if(cms_good_muons[j].DeltaR(cms_good_jets[i]) < 0.4){
              cms_good_muons.erase(cms_good_muons.begin() + j);
              cms_good_muons_charge.erase(cms_good_muons_charge.begin() + j);              
              --j;
            }
          }
        }
       
        b_cms_lep_n = cms_good_electrons.size() + cms_good_muons.size();
        std::vector<TLorentzVector> cms_leptons_pos; /// For NuW
        std::vector<TLorentzVector> cms_leptons_neg; /// For NuW
        for(uint i = 0; i < cms_good_electrons.size(); ++i){
          TLorentzVector temp;
          temp.SetPtEtaPhiE(cms_good_electrons[i].Pt(), cms_good_electrons[i].Eta(), cms_good_electrons[i].Phi(), cms_good_electrons[i].E());
          b_cms_lep_pt.push_back(     cms_good_electrons[i].Pt());   
          b_cms_lep_eta.push_back(    cms_good_electrons[i].Eta());  
          b_cms_lep_phi.push_back(    cms_good_electrons[i].Phi()); 
          b_cms_lep_e.push_back(      cms_good_electrons[i].E());  
          b_cms_lep_charge.push_back( cms_good_electrons_charge[i]);
          b_cms_lep_type.push_back(11*cms_good_electrons_charge[i]);
          if(cms_good_electrons_charge[i] > 0){
            cms_leptons_pos.push_back(temp);
          } else {
            cms_leptons_neg.push_back(temp);
          }
        }
        for(uint i = 0; i < cms_good_muons.size(); ++i){
          /// Not adding muons to NuW stuff yet due to RIVET bug!
          TLorentzVector temp;
          temp.SetPtEtaPhiE(cms_good_muons[i].Pt(), cms_good_muons[i].Eta(), cms_good_muons[i].Phi(), cms_good_muons[i].E());
          b_cms_lep_pt.push_back(     cms_good_muons[i].Pt());   
          b_cms_lep_eta.push_back(    cms_good_muons[i].Eta());  
          b_cms_lep_phi.push_back(    cms_good_muons[i].Phi()); 
          b_cms_lep_e.push_back(      cms_good_muons[i].E());  
          b_cms_lep_charge.push_back( cms_good_muons_charge[i]);
          b_cms_lep_type.push_back(13*cms_good_muons_charge[i]);
          if(cms_good_muons_charge[i] > 0){
            cms_leptons_pos.push_back(temp);
          } else {
            cms_leptons_neg.push_back(temp);
          }           
        }   
        for(uint i = 0; i < cms_good_jets.size(); ++i){
          b_cms_jet_pt.push_back(     cms_good_jets[i].Pt());   
          b_cms_jet_eta.push_back(    cms_good_jets[i].Eta());  
          b_cms_jet_phi.push_back(    cms_good_jets[i].Phi()); 
          b_cms_jet_e.push_back(      cms_good_jets[i].E());  
        } 
        std::vector<TLorentzVector> cms_bjets;
        for(uint i = 0; i < cms_good_bjets.size(); ++i){
          TLorentzVector temp;
          temp.SetPtEtaPhiE(cms_good_bjets[i].Pt(), cms_good_bjets[i].Eta(), cms_good_bjets[i].Phi(), cms_good_bjets[i].E());
          b_cms_bjet_pt.push_back(    cms_good_bjets[i].Pt());   
          b_cms_bjet_eta.push_back(   cms_good_bjets[i].Eta());  
          b_cms_bjet_phi.push_back(   cms_good_bjets[i].Phi()); 
          b_cms_bjet_e.push_back(     cms_good_bjets[i].E()); 
          cms_bjets.push_back(temp); 
        } 

        b_cms_met_et   = m_met_cms_met;
        b_cms_met_phi  = m_met_cms.phi() - TMath::Pi();
        b_cms_met_ex   = TMath::Cos(b_cms_met_phi)*m_met_cms_met;
        b_cms_met_ey   = TMath::Sin(b_cms_met_phi)*m_met_cms_met;

        /////////////////////////////////////
        ////      EVENT SELECTION        ////
        /////////////////////////////////////

        /*if(atlas_leptons_pos.size() > 0 && atlas_leptons_neg.size() > 0 && atlas_bjets.size() > 1){
          NeutrinoWeighter nu = NeutrinoWeighter();
          double b_atlas_weight_max = nu.Reconstruct(atlas_leptons_pos, atlas_leptons_neg, atlas_bjets[0], atlas_bjets[1], b_atlas_met_ex, b_atlas_met_ey, b_atlas_met_phi);

          if(b_atlas_weight_max > 0){
            TLorentzVector top = nu.GetTop();
            b_atlas_top_pt  = top.Pt();
            b_atlas_top_eta = top.Eta();
            b_atlas_top_phi = top.Phi();
            b_atlas_top_e   = top.E();
            b_atlas_top_m   = top.M();  
          }                            
        }*/

        output_tree_reco->Fill();
      }    


      
      //////////////////////////////////////////////
      ///--          HELPER FUNCTIONS         ---///
      //////////////////////////////////////////////

      void ParticleReconstructionDilepton(vector<DressedLepton> electrons, vector<DressedLepton> muons, vector<TLorentzVector> ljets, vector<TLorentzVector> bjets, vector<TLorentzVector> neutrinos){

        ///-- Some safety requirements --///
        if(neutrinos.size() < 2)
          return;

        if(bjets.size() < 2)
          return;

        vector<DressedLepton> leptons = electrons + muons;      
        if(leptons.size() != 2)
          return;

        if(leptons[0].charge()*leptons[1].charge() > 0)
          return;

        TLorentzVector lepton_pos;
        TLorentzVector lepton_neg;
        int lepton_pos_type = -99;
        int lepton_neg_type = -99;  



        if(leptons[0].charge() > 0){
          lepton_pos = ConvertParticle(leptons[0]); 
          lepton_neg = ConvertParticle(leptons[1]);
          lepton_pos_type = leptons[0].pid();
          lepton_neg_type = leptons[1].pid();          
        } else {
          lepton_pos = ConvertParticle(leptons[1]);
          lepton_neg = ConvertParticle(leptons[0]);
          lepton_pos_type = leptons[1].pid();
          lepton_neg_type = leptons[0].pid(); 
        }

        ///-- If there are more than two bjets, take the two highest in pT (they are already pT ordered)--///
        TLorentzVector b1 = bjets[0];
        TLorentzVector b2 = bjets[1];

        ///-- If there are more than two neutrinos, take the two highest in pT (they are already pT ordered) --///
        TLorentzVector neutrino_1 = neutrinos[0];
        TLorentzVector neutrino_2 = neutrinos[1];        

        ///-- Pseudotop reconstruction from LHCTopWG --///
        TLorentzVector top;
        TLorentzVector tbar;
        TLorentzVector b;
        TLorentzVector bbar;
        TLorentzVector nu;
        TLorentzVector nubar;
        TLorentzVector Wp_a, Wm_a, Wp_b, Wm_b, Wp, Wm, top_a, tbar_a, top_b, tbar_b;

        Wp_a = lepton_pos + neutrino_1;
        Wm_a = lepton_neg + neutrino_2;
        Wp_b = lepton_pos + neutrino_2;
        Wm_b = lepton_neg + neutrino_1;

        double diff_a(0), diff_b(0);
        diff_a = fabs(Wp_a.M() - 80.1) + fabs(Wm_a.M() - 80.1);
        diff_b = fabs(Wp_b.M() - 80.1) + fabs(Wm_b.M() - 80.1);

        if(diff_a < diff_b){ // A is the correct combination
          Wp    = Wp_a;
          Wm    = Wm_a;
          nu    = neutrino_1;
          nubar = neutrino_2;
        } else if (diff_b < diff_a){ // B is the right combination
          Wp    = Wp_b;
          Wm    = Wm_b;
          nu    = neutrino_2;
          nubar = neutrino_1;
        } else {
          Wp    = Wp_b;
          Wm    = Wm_b;
          nu    = neutrino_2;
          nubar = neutrino_1;
        }

        top_a  = Wp + b1;
        tbar_a = Wm + b2;

        top_b  = Wp + b2;
        tbar_b = Wm + b1;

        diff_a = fabs(top_a.M() - 172.5) + fabs(tbar_a.M() - 172.5);
        diff_b = fabs(top_b.M() - 172.5) + fabs(tbar_b.M() - 172.5);

        if(diff_a < diff_b){ // A is the correct combination
          top   = top_a;
          tbar  = tbar_a;
          b     = b1;
          bbar  = b2;
        } else if (diff_a > diff_b){ // B is the right combination
          top   = top_b;
          tbar  = tbar_b;
          b     = b2;
          bbar  = b1;
        } else {
          top   = top_a;
          tbar  = tbar_a;
          b     = b1;
          bbar  = b2;
        }

        TLorentzVector ttbar = top + tbar;

        b_particle_top_pt           = top.Pt();  
        b_particle_top_eta          = top.Eta();   
        b_particle_top_phi          = top.Phi();   
        b_particle_top_m            = top.M(); 
        b_particle_top_e            = top.E(); 
        b_particle_top_y            = top.Rapidity(); 
        b_particle_tbar_pt          = tbar.Pt();   
        b_particle_tbar_eta         = tbar.Eta();    
        b_particle_tbar_phi         = tbar.Phi();    
        b_particle_tbar_m           = tbar.M();  
        b_particle_tbar_e           = tbar.E();  
        b_particle_tbar_y           = tbar.Rapidity();  
        b_particle_ttbar_pt         = ttbar.Pt();    
        b_particle_ttbar_eta        = ttbar.Eta();     
        b_particle_ttbar_phi        = ttbar.Phi();     
        b_particle_ttbar_m          = ttbar.M();   
        b_particle_ttbar_e          = ttbar.E();   
        b_particle_ttbar_y          = ttbar.Rapidity(); 

        b_particle_cos_helicity_p   = cos_theta_helicity(   top, top,  ttbar, lepton_pos, +1);
        b_particle_cos_helicity_m   = cos_theta_helicity(   top, tbar, ttbar, lepton_neg, -1);
        b_particle_cos_transverse_p = cos_theta_transverse( top, top,  ttbar, lepton_pos, +1);
        b_particle_cos_transverse_m = cos_theta_transverse( top, tbar, ttbar, lepton_neg, -1);
        b_particle_cos_raxis_p      = cos_theta_raxis(      top, top,  ttbar, lepton_pos, +1);
        b_particle_cos_raxis_m      = cos_theta_raxis(      top, tbar, ttbar, lepton_neg, -1);
    
        b_particle_lep_p_pt         = lepton_pos.Pt();  
        b_particle_lep_p_eta        = lepton_pos.Eta(); 
        b_particle_lep_p_phi        = lepton_pos.Phi(); 
        b_particle_lep_p_e          = lepton_pos.E();   
        b_particle_lep_p_type       = lepton_pos_type; 

        b_particle_lep_m_pt         = lepton_neg.Pt();  
        b_particle_lep_m_eta        = lepton_neg.Eta(); 
        b_particle_lep_m_phi        = lepton_neg.Phi(); 
        b_particle_lep_m_e          = lepton_neg.E();   
        b_particle_lep_m_type       = lepton_neg_type;   

        b_particle_b_pt             = b.Pt();          
        b_particle_b_eta            = b.Eta(); 
        b_particle_b_phi            = b.Phi(); 
        b_particle_b_e              = b.E();   

        b_particle_bbar_pt          = bbar.Pt();          
        b_particle_bbar_eta         = bbar.Eta(); 
        b_particle_bbar_phi         = bbar.Phi(); 
        b_particle_bbar_e           = bbar.E(); 

        b_particle_nu_pt            = nu.Pt();          
        b_particle_nu_eta           = nu.Eta(); 
        b_particle_nu_phi           = nu.Phi(); 
        b_particle_nu_e             = nu.E();   

        b_particle_nubar_pt         = nubar.Pt();          
        b_particle_nubar_eta        = nubar.Eta(); 
        b_particle_nubar_phi        = nubar.Phi(); 
        b_particle_nubar_e          = nubar.E(); 

        return;
      }

      void ParticleReconstructionTrilepton(vector<DressedLepton> electrons, vector<DressedLepton> muons, vector<TLorentzVector> ljets, vector<TLorentzVector> bjets, vector<TLorentzVector> neutrinos){

        ///-- The real question is, who cares about ttW. Not me apparently --///

        return;
      }

      void ParticleReconstructionTetralepton(vector<DressedLepton> electrons, vector<DressedLepton> muons, vector<TLorentzVector> ljets, vector<TLorentzVector> bjets, vector<TLorentzVector> neutrinos){
        
        ///-- Some safety requirements --///
        /*if(neutrinos.size() < 2)
          return;
        if(leptons.size() != 4)
          return;
        if(bjets.size() < 2)
          return;

        Particle lepton_pos_fromZ;
        Particle lepton_neg_fromZ;
        Particle lepton_pos_fromTop;
        Particle lepton_neg_fromTop;        

        int lepton_pos_type_fromZ;
        int lepton_neg_type_fromZ; 
        int lepton_pos_type_fromTop;
        int lepton_neg_type_fromTop;         
      

        int lepton_pos_index_fromZ;
        int lepton_neg_index_fromZ; 
        int lepton_pos_index_fromTop;
        int lepton_neg_index_fromTop;

        ///-- Find the pair of OS leptons closest in mass to the Z boson --/// 
        float min_Z_mass_diff = 999999.;
        foreach(DressedLepton lepton1, leptons){
          foreach(DressedLepton lepton2, leptons){
            if(lepton1 == lepton2) continue;
            if(lepton1.charge()*lepton2.charge() > 0) continue;
            FourMomentum tempZ = lepton1.momentum() + lepton2.momentum();
            if(fabs(tempZ.mass() - 91.0) < min_Z_mass_diff){
              min_Z_mass_diff = fabs(tempZ.mass() - 91.0);
              if(lepton1.charge() > 0){
                lepton_pos_fromZ = lepton1;
                lepton_neg_fromZ = lepton2;
              } else {
                lepton_pos_fromZ = lepton2;
                lepton_neg_fromZ = lepton1;   
              }
            }
          }
        }


        /* ///-- TETRALEPTON CASE --///
        if (els_good.size() + mus_good.size() == 4){
          int el_index = -1;
          int mu_index = -1;

          if (els_good.size() == 4 || mus_good.size() == 4){
            pass_lepton_selecton = false; /// Don't consider this case due to ttbar backgrounds
          }
          if (els_good.size() == 1 && mus_good.size() == 3){
            /// assume electron coming from the top ///
            FourMomentum m0m1 = mus_good[0].momentum() + mus_good[1].momentum();
            FourMomentum m1m2 = mus_good[1].momentum() + mus_good[2].momentum();
            FourMomentum m0m2 = mus_good[0].momentum() + mus_good[2].momentum();

            bool diff_m0m1 = fabs(m0m1.mass() - 91.0) < 10.;
            bool diff_m1m2 = fabs(m1m2.mass() - 91.0) < 10.;
            bool diff_m0m2 = fabs(m0m2.mass() - 91.0) < 10.;

            int el_index = 0;
            if (diff_m0m1 < diff_m1m2 && diff_m0m1 < diff_m0m2){
              int mu_index = 2;
            } else if (diff_m1m2 < diff_m0m1 && diff_m1m2 < diff_m0m2){
              int mu_index = 0;
            } else if (diff_m0m2 < diff_m0m1 && diff_m0m2 < diff_m1m2){
              int mu_index = 1;              
            }
          }
          if (mus_good.size() == 1 && els_good.size() == 3){
            /// assume muon coming from the top ///
            FourMomentum e0e1 = els_good[0].momentum() + els_good[1].momentum();
            FourMomentum e1e2 = els_good[1].momentum() + els_good[2].momentum();
            FourMomentum e0e2 = els_good[0].momentum() + els_good[2].momentum();

            bool diff_e0e1 = fabs(e0e1.mass() - 91.0);
            bool diff_e1e2 = fabs(e1e2.mass() - 91.0);
            bool diff_e0e2 = fabs(e0e2.mass() - 91.0);

            int mu_index = 0;
            if (diff_e0e1 < diff_e1e2 && diff_e0e1 < diff_e0e2){
              int el_index = 2;
            } else if (diff_e1e2 < diff_e0e1 && diff_e1e2 < diff_e0e2){
              int el_index = 0;
            } else if (diff_e0e2 < diff_e0e1 && diff_e0e2 < diff_e1e2){
              int el_index = 1;              
            }
          }
          if (mus_good.size() == 2 && els_good.size() == 2){
            pass_lepton_selecton = false;
          }

          if (els_good[el_index].charge() * mus_good[mu_index].charge() > 0){
            pass_lepton_selecton = false;
          }

          if (els_good[el_index].charge() > 0){
            m_particle_lepton_pos = FourMomentum(els_good[el_index].momentum().E(), els_good[el_index].momentum().px(), els_good[el_index].momentum().py(), els_good[el_index].momentum().pz());
            m_particle_lepton_neg = FourMomentum(mus_good[mu_index].momentum().E(), mus_good[mu_index].momentum().px(), mus_good[mu_index].momentum().py(), mus_good[mu_index].momentum().pz());          
            m_particle_lepton_pos_type = -11;
            m_particle_lepton_neg_type =  13;
          } else {
            m_particle_lepton_neg = FourMomentum(els_good[el_index].momentum().E(), els_good[el_index].momentum().px(), els_good[el_index].momentum().py(), els_good[el_index].momentum().pz());
            m_particle_lepton_pos = FourMomentum(mus_good[mu_index].momentum().E(), mus_good[mu_index].momentum().px(), mus_good[mu_index].momentum().py(), mus_good[mu_index].momentum().pz());          
            m_particle_lepton_pos_type = -13;
            m_particle_lepton_neg_type =  11;          
          }

        }*/

        return;
      }

      TLorentzVector ConvertParticle(Particle p){
        TLorentzVector temp;

        double phi = p.phi();
        if(phi > TMath::Pi()){
          double diff = phi - TMath::Pi();
          phi = -TMath::Pi() + diff;
        }

        temp.SetPtEtaPhiE(p.pT(), p.eta(), phi, p.E());
        return temp;
      }

      TLorentzVector ConvertParticle(FourMomentum p){
        TLorentzVector temp;

        double phi = p.phi();
        if(phi > TMath::Pi()){
          double diff = phi - TMath::Pi();
          phi = -TMath::Pi() + diff;
        }

        temp.SetPtEtaPhiE(p.pT(), p.eta(), phi, p.E());
        return temp;
      }

      float cos_theta_helicity(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

        TVector3 boost_to_ttbar = ttbar.BoostVector();
        boost_to_ttbar *= -1.;
    
        parent_t.Boost(boost_to_ttbar);
        top.Boost(boost_to_ttbar);
        lep.Boost(boost_to_ttbar);
    
        TVector3 boost_to_parent_t = parent_t.BoostVector();
        boost_to_parent_t *= -1.;

        lep.Boost(boost_to_parent_t);
  
        TVector3 k_vector = top.Vect().Unit();
        k_vector *= sign;
        float theta = lep.Vect().Unit().Dot(k_vector);  

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      float cos_theta_raxis(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

        TVector3 boost_to_ttbar = ttbar.BoostVector();
        boost_to_ttbar *= -1.;
    
        parent_t.Boost(boost_to_ttbar);
        top.Boost(boost_to_ttbar);
        lep.Boost(boost_to_ttbar);
    
        TVector3 boost_to_parent_t = parent_t.BoostVector();
        boost_to_parent_t *= -1.;

        lep.Boost(boost_to_parent_t);
  
        TVector3 k_vector = top.Vect().Unit();
        k_vector *= sign;
        TVector3 p_vector(0,0,1);

        float y = p_vector.Dot(k_vector);
        float r = pow((1. - y*y),0.5);

        TVector3 r_vector = (1./r)*(p_vector - y*k_vector);
        if (sign == 1){
          ///-- We're in the a axis --///
          if(y > 0) r_vector *= 1.;
          if(y < 0) r_vector *= -1.;
        } else if (sign == -1){
          ///-- We're in the b axis --///
          if(y > 0) r_vector *= -1.;
          if(y < 0) r_vector *= 1.;
        }

        float theta = lep.Vect().Unit().Dot(r_vector);

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      float cos_theta_transverse(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

        TVector3 boost_to_ttbar = ttbar.BoostVector();
        boost_to_ttbar *= -1.;
    
        parent_t.Boost(boost_to_ttbar);
        top.Boost(boost_to_ttbar);
        lep.Boost(boost_to_ttbar);
    
        TVector3 boost_to_parent_t = parent_t.BoostVector();
        boost_to_parent_t *= -1.;

        lep.Boost(boost_to_parent_t);
  
        TVector3 k_vector = top.Vect().Unit();
        k_vector *= sign;
        TVector3 p_vector(0,0,1);

        float y = p_vector.Dot(k_vector);
        float r = pow((1. - y*y),0.5);

        TVector3 n_vector = (1./r)*(p_vector.Cross(k_vector)); ///-- Should this be Unit Vector? --///

        if (sign == 1){
          ///-- We're in the a axis --///
          if(y > 0) n_vector *= 1.;
          if(y < 0) n_vector *= -1.;
        } else if (sign == -1){
          ///-- We're in the b axis --///
          if(y > 0) n_vector *= -1.;
          if(y < 0) n_vector *= 1.;
        }

        float theta = lep.Vect().Unit().Dot(n_vector);

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      float opening_angle(TLorentzVector ttbar, TLorentzVector lep_p, TLorentzVector lep_n){

        TVector3 boost_to_ttbar = ttbar.BoostVector();
        boost_to_ttbar *= -1.;
    
        lep_p.Boost(boost_to_ttbar);
        lep_n.Boost(boost_to_ttbar);  

        float phi = lep_p.DeltaPhi(lep_n);
        return phi;
      }


	    float cos_theta_helicity(const FourMomentum top, const FourMomentum parent_t, const FourMomentum ttbar, const FourMomentum lep, float sign){

        const FourMomentum parent_t_in_ttbar_rf = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(parent_t);
        const FourMomentum top_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(top);
        const FourMomentum lep_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(lep);        

        const FourMomentum lep_in_parent_t_rf   = LorentzTransform::mkFrameTransformFromBeta(parent_t_in_ttbar_rf.betaVec()).transform(lep_in_ttbar_rf);
 
        Vector3 k_vector = top_in_ttbar_rf.vector3().unit();
        k_vector *= sign;
        float theta = lep_in_parent_t_rf.vector3().unit().dot(k_vector);

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      float cos_theta_raxis(const FourMomentum top, const FourMomentum parent_t, const FourMomentum ttbar, const FourMomentum lep, float sign){

        const FourMomentum parent_t_in_ttbar_rf = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(parent_t);
        const FourMomentum top_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(top);
        const FourMomentum lep_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(lep);        

        const FourMomentum lep_in_parent_t_rf   = LorentzTransform::mkFrameTransformFromBeta(parent_t_in_ttbar_rf.betaVec()).transform(lep_in_ttbar_rf);
 
        Vector3 k_vector = top_in_ttbar_rf.vector3().unit();
        k_vector *= sign;
        Vector3 p_vector(0,0,1);

        float y = p_vector.dot(k_vector);
        float r = pow((1. - y*y),0.5);

        Vector3 r_vector = (1./r)*(p_vector - y*k_vector);
        if (sign == 1){
          ///-- We're in the a axis --///
          if(y > 0) r_vector *= 1.;
          if(y < 0) r_vector *= -1.;
        } else if (sign == -1){
          ///-- We're in the b axis --///
          if(y > 0) r_vector *= -1.;
          if(y < 0) r_vector *= 1.;
        }
        float theta = lep_in_parent_t_rf.vector3().unit().dot(r_vector);

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      float cos_theta_transverse(const FourMomentum top, const FourMomentum parent_t, const FourMomentum ttbar, const FourMomentum lep, float sign){

        const FourMomentum parent_t_in_ttbar_rf = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(parent_t);
        const FourMomentum top_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(top);
        const FourMomentum lep_in_ttbar_rf      = LorentzTransform::mkFrameTransformFromBeta(ttbar.betaVec()).transform(lep);        

        const FourMomentum lep_in_parent_t_rf   = LorentzTransform::mkFrameTransformFromBeta(parent_t_in_ttbar_rf.betaVec()).transform(lep_in_ttbar_rf);
 
        Vector3 k_vector = top_in_ttbar_rf.vector3().unit();
        k_vector *= sign;
        Vector3 p_vector(0,0,1);

        float y = p_vector.dot(k_vector);
        float r = pow((1. - y*y),0.5);

        Vector3 n_vector = (1./r)*(p_vector.cross(k_vector)); ///-- Should this be Unit Vector? --///

        if (sign == 1){
          ///-- We're in the a axis --///
          if(y > 0) n_vector *= 1.;
          if(y < 0) n_vector *= -1.;
        } else if (sign == -1){
          ///-- We're in the b axis --///
          if(y > 0) n_vector *= -1.;
          if(y < 0) n_vector *= 1.;
        }
        float theta = lep_in_parent_t_rf.vector3().unit().dot(n_vector);

        //-- If we have a nan move to this to the overflow bins --//
        if (isnan(theta)){
          return -55.;
        } else {
          return theta;
        }
      }

      vector<DressedLepton> SelectParticleLeptons(vector<DressedLepton> leptons){
          
        vector<DressedLepton> return_vec;

        foreach(DressedLepton lep, leptons){ 
          if(lep.momentum().pT() < 25. || fabs(lep.momentum().eta()) > 2.5)
            continue;
          return_vec.push_back(lep);
        }
        return return_vec;
      }

      Jets SelectParticleLightJets(const Jets &jets){
        Jets ljets;
  
        foreach (Jet jet, jets){
          if (!jet.bTagged()){
            ljets.push_back(jet);
          }
        }
        return ljets;
      }      

      Jets SelectParticleBJets(const Jets &jets){
        Jets bjets;
  
        foreach (Jet jet, jets){
          if (jet.bTagged()){
            bjets.push_back(jet);
          }
        }
        return bjets;
      }   


      bool SelectJets(const Jets &alljets, FourMomentum &jet1, FourMomentum &jet2){

        Jets bjets;
        Jets ljets;
  
        foreach (Jet jet, alljets){
          if (jet.bTagged()){
            bjets.push_back(jet);
          } else {
            ljets.push_back(jet);
          }
        }

        if (bjets.size() < 1)
          return true;
        if ((bjets.size() + ljets.size()) < 2)
          return true;

        if(bjets.size() > 1){
          jet1 = FourMomentum(bjets[0].momentum().E(), bjets[0].momentum().px(), bjets[0].momentum().py(), bjets[0].momentum().pz());
          jet2 = FourMomentum(bjets[1].momentum().E(), bjets[1].momentum().px(), bjets[1].momentum().py(), bjets[1].momentum().pz());        
        } else {
          jet1 = FourMomentum(bjets[0].momentum().E(), bjets[0].momentum().px(), bjets[0].momentum().py(), bjets[0].momentum().pz());
          jet2 = FourMomentum(ljets[0].momentum().E(), ljets[0].momentum().px(), ljets[0].momentum().py(), ljets[0].momentum().pz());
        }
        return false;
      }

      bool SelectNeutrinos(Particles &neutrinos, FourMomentum &nu1, FourMomentum &nu2){

        if( neutrinos.size()<2)
          return true;

        foreach (Particle neutrino, neutrinos){
          if(nu1.pT() == 0 && nu2.pT() == 0){
            nu1 = FourMomentum(neutrino.momentum().E(), neutrino.momentum().px(), neutrino.momentum().py(), neutrino.momentum().pz());
          } else if (nu1.pT() !=0 && nu2.pT() == 0){
            nu2 = FourMomentum(neutrino.momentum().E(), neutrino.momentum().px(), neutrino.momentum().py(), neutrino.momentum().pz());
          } else {
            if(neutrino.pT() > nu1.pT() && nu1.pT() > nu2.pT()){
              nu2 = FourMomentum(neutrino.momentum().E(), neutrino.momentum().px(), neutrino.momentum().py(), neutrino.momentum().pz());            
            }
            if(neutrino.pT() > nu1.pT() && nu1.pT() < nu2.pT()){
              nu1 = FourMomentum(neutrino.momentum().E(), neutrino.momentum().px(), neutrino.momentum().py(), neutrino.momentum().pz());            
            }
          }
        }
        return false;
      }


      void setBranchesParton(TTree* output_tree_parton){

        output_tree_parton->Branch(   "d_event_weight",           &b_parton_event_weight,       "d_event_weight/F");
        output_tree_parton->Branch(   "d_event_number",           &b_parton_event_number,       "d_event_number/F");
        output_tree_parton->Branch(   "d_top_pt",                 &b_parton_top_pt,             "d_top_pt/F");
        output_tree_parton->Branch(   "d_top_eta",                &b_parton_top_eta,            "d_top_eta/F");
        output_tree_parton->Branch(   "d_top_phi",                &b_parton_top_phi,            "d_top_phi/F");
        output_tree_parton->Branch(   "d_top_m",                  &b_parton_top_m,              "d_top_m/F");
        output_tree_parton->Branch(   "d_top_e",                  &b_parton_top_e,              "d_top_e/F");
        output_tree_parton->Branch(   "d_top_y",                  &b_parton_top_y,              "d_top_y/F");
        output_tree_parton->Branch(   "d_tbar_pt",                &b_parton_tbar_pt,            "d_tbar_pt/F");
        output_tree_parton->Branch(   "d_tbar_eta",               &b_parton_tbar_eta,           "d_tbar_eta/F");
        output_tree_parton->Branch(   "d_tbar_phi",               &b_parton_tbar_phi,           "d_tbar_phi/F");
        output_tree_parton->Branch(   "d_tbar_m",                 &b_parton_tbar_m,             "d_tbar_m/F");
        output_tree_parton->Branch(   "d_tbar_e",                 &b_parton_tbar_e,             "d_tbar_e/F");
        output_tree_parton->Branch(   "d_tbar_y",                 &b_parton_tbar_y,             "d_tbar_y/F");
        output_tree_parton->Branch(   "d_ttbar_pt",               &b_parton_ttbar_pt,           "d_ttbar_pt/F");
        output_tree_parton->Branch(   "d_ttbar_eta",              &b_parton_ttbar_eta,          "d_ttbar_eta/F");
        output_tree_parton->Branch(   "d_ttbar_phi",              &b_parton_ttbar_phi,          "d_ttbar_phi/F");
        output_tree_parton->Branch(   "d_ttbar_m",                &b_parton_ttbar_m,            "d_ttbar_m/F");
        output_tree_parton->Branch(   "d_ttbar_e",                &b_parton_ttbar_e,            "d_ttbar_e/F");
        output_tree_parton->Branch(   "d_ttbar_y",                &b_parton_ttbar_y,            "d_ttbar_u/F");
        output_tree_parton->Branch(   "d_cos_helicity_p",         &b_parton_cos_helicity_p,     "d_cos_helicity_p/F");
        output_tree_parton->Branch(   "d_cos_helicity_m",         &b_parton_cos_helicity_m,     "d_cos_helicity_m/F");
        output_tree_parton->Branch(   "d_cos_transverse_p",       &b_parton_cos_transverse_p,   "d_cos_transverse_p/F");        
        output_tree_parton->Branch(   "d_cos_transverse_m",       &b_parton_cos_transverse_m,   "d_cos_transverse_m/F");                
        output_tree_parton->Branch(   "d_cos_raxis_p",            &b_parton_cos_raxis_p,        "d_cos_raxis_p/F");
        output_tree_parton->Branch(   "d_cos_raxis_m",            &b_parton_cos_raxis_m,        "d_cos_raxis_m/F");       
        output_tree_parton->Branch(   "d_boson_pt",               &b_parton_boson_pt,           "d_boson_pt/F");
        output_tree_parton->Branch(   "d_boson_eta",              &b_parton_boson_eta,          "d_boson_eta/F");
        output_tree_parton->Branch(   "d_boson_phi",              &b_parton_boson_phi,          "d_boson_phi/F");
        output_tree_parton->Branch(   "d_boson_m",                &b_parton_boson_m,            "d_boson_m/F");
        output_tree_parton->Branch(   "d_boson_e",                &b_parton_boson_e,            "d_boson_e/F");
        output_tree_parton->Branch(   "d_boson_y",                &b_parton_boson_y,            "d_boson_y/F");
        output_tree_parton->Branch(   "d_lep_p_pt",               &b_parton_lep_p_pt,           "d_lep_p_pt/F");  
        output_tree_parton->Branch(   "d_lep_p_eta",              &b_parton_lep_p_eta,          "d_lep_p_eta/F"); 
        output_tree_parton->Branch(   "d_lep_p_phi",              &b_parton_lep_p_phi,          "d_lep_p_phi/F"); 
        output_tree_parton->Branch(   "d_lep_p_e",                &b_parton_lep_p_e,            "d_lep_p_e/F");   
        output_tree_parton->Branch(   "d_lep_m_pt",               &b_parton_lep_m_pt,           "d_lep_m_pt/F");   
        output_tree_parton->Branch(   "d_lep_m_eta",              &b_parton_lep_m_eta,          "d_lep_m_eta/F");  
        output_tree_parton->Branch(   "d_lep_m_phi",              &b_parton_lep_m_phi,          "d_lep_m_phi/F"); 
        output_tree_parton->Branch(   "d_lep_m_e",                &b_parton_lep_m_e,            "d_lep_m_e/F");
        output_tree_parton->Branch(   "d_b_pt",                   &b_parton_b_pt,               "d_b_pt/F");          
        output_tree_parton->Branch(   "d_b_eta",                  &b_parton_b_eta,              "d_b_eta/F"); 
        output_tree_parton->Branch(   "d_b_phi",                  &b_parton_b_phi,              "d_b_phi/F"); 
        output_tree_parton->Branch(   "d_b_e",                    &b_parton_b_e,                "d_b_m/F");   
        output_tree_parton->Branch(   "d_bbar_pt",                &b_parton_bbar_pt,            "d_bbar_pt/F");   
        output_tree_parton->Branch(   "d_bbar_eta",               &b_parton_bbar_eta,           "d_bbar_eta/F");  
        output_tree_parton->Branch(   "d_bbar_phi",               &b_parton_bbar_phi,           "d_bbar_phi/F"); 
        output_tree_parton->Branch(   "d_bbar_e",                 &b_parton_bbar_e,             "d_bbar_e/F");
        output_tree_parton->Branch(   "d_nu_pt",                  &b_parton_nu_pt,              "d_nu_pt/F");          
        output_tree_parton->Branch(   "d_nu_eta",                 &b_parton_nu_eta,             "d_nu_eta/F"); 
        output_tree_parton->Branch(   "d_nu_phi",                 &b_parton_nu_phi,             "d_nu_phi/F"); 
        output_tree_parton->Branch(   "d_nu_e",                   &b_parton_nu_e,               "d_nu_m/F");   
        output_tree_parton->Branch(   "d_nubar_pt",               &b_parton_nubar_pt,           "d_nubar_pt/F");   
        output_tree_parton->Branch(   "d_nubar_eta",              &b_parton_nubar_eta,          "d_nubar_eta/F");  
        output_tree_parton->Branch(   "d_nubar_phi",              &b_parton_nubar_phi,          "d_nubar_phi/F"); 
        output_tree_parton->Branch(   "d_nubar_e",                &b_parton_nubar_e,            "d_nubar_e/F");
        output_tree_parton->Branch(   "d_met_et",                 &b_parton_met_et,             "d_met_et/F");
        output_tree_parton->Branch(   "d_met_phi",                &b_parton_met_phi,            "d_met_phi/F");
        output_tree_parton->Branch(   "d_met_ex",                 &b_parton_met_ex,             "d_met_ex/F");
        output_tree_parton->Branch(   "d_met_ey",                 &b_parton_met_ey,             "d_met_ey/F"); 
      }

      void setBranchesParticle(TTree* output_tree_particle){
        output_tree_particle->Branch( "d_event_weight",           &b_particle_event_weight,     "d_event_weight/F");
        output_tree_particle->Branch( "d_event_number",           &b_particle_event_number,     "d_event_number/F");
        output_tree_particle->Branch( "d_top_pt",                 &b_particle_top_pt,           "d_top_pt/F");
        output_tree_particle->Branch( "d_top_eta",                &b_particle_top_eta,          "d_top_eta/F");
        output_tree_particle->Branch( "d_top_phi",                &b_particle_top_phi,          "d_top_phi/F");
        output_tree_particle->Branch( "d_top_m",                  &b_particle_top_m,            "d_top_m/F");
        output_tree_particle->Branch( "d_top_e",                  &b_particle_top_e,            "d_top_e/F");
        output_tree_particle->Branch( "d_top_y",                  &b_particle_top_y,            "d_top_y/F");
        output_tree_particle->Branch( "d_tbar_pt",                &b_particle_tbar_pt,          "d_tbar_pt/F");
        output_tree_particle->Branch( "d_tbar_eta",               &b_particle_tbar_eta,         "d_tbar_eta/F");
        output_tree_particle->Branch( "d_tbar_phi",               &b_particle_tbar_phi,         "d_tbar_phi/F");
        output_tree_particle->Branch( "d_tbar_m",                 &b_particle_tbar_m,           "d_tbar_m/F");
        output_tree_particle->Branch( "d_tbar_e",                 &b_particle_tbar_e,           "d_tbar_e/F");
        output_tree_particle->Branch( "d_tbar_y",                 &b_particle_tbar_y,           "d_tbar_y/F");
        output_tree_particle->Branch( "d_ttbar_pt",               &b_particle_ttbar_pt,         "d_ttbar_pt/F");
        output_tree_particle->Branch( "d_ttbar_eta",              &b_particle_ttbar_eta,        "d_ttbar_eta/F");
        output_tree_particle->Branch( "d_ttbar_phi",              &b_particle_ttbar_phi,        "d_ttbar_phi/F");
        output_tree_particle->Branch( "d_ttbar_m",                &b_particle_ttbar_m,          "d_ttbar_m/F");
        output_tree_particle->Branch( "d_ttbar_e",                &b_particle_ttbar_e,          "d_ttbar_e/F");
        output_tree_particle->Branch( "d_ttbar_y",                &b_particle_ttbar_y,          "d_ttbar_u/F");
        output_tree_particle->Branch( "d_cos_helicity_p",         &b_particle_cos_helicity_p,   "d_cos_helicity_p/F");
        output_tree_particle->Branch( "d_cos_helicity_m",         &b_particle_cos_helicity_m,   "d_cos_helicity_m/F");
        output_tree_particle->Branch( "d_cos_transverse_p",       &b_particle_cos_transverse_p, "d_cos_transverse_p/F");        
        output_tree_particle->Branch( "d_cos_transverse_m",       &b_particle_cos_transverse_m, "d_cos_transverse_m/F");               
        output_tree_particle->Branch( "d_cos_raxis_p",            &b_particle_cos_raxis_p,      "d_cos_raxis_p/F");
        output_tree_particle->Branch( "d_cos_raxis_m",            &b_particle_cos_raxis_m,      "d_cos_raxis_m/F");
        output_tree_particle->Branch( "d_boson_pt",               &b_particle_boson_pt,         "d_boson_pt/F");
        output_tree_particle->Branch( "d_boson_eta",              &b_particle_boson_eta,        "d_boson_eta/F");
        output_tree_particle->Branch( "d_boson_phi",              &b_particle_boson_phi,        "d_boson_phi/F");
        output_tree_particle->Branch( "d_boson_m",                &b_particle_boson_m,          "d_boson_m/F");
        output_tree_particle->Branch( "d_boson_e",                &b_particle_boson_e,          "d_boson_e/F");
        output_tree_particle->Branch( "d_boson_y",                &b_particle_boson_y,          "d_boson_y/F");
        output_tree_particle->Branch( "d_lep_p_pt",               &b_particle_lep_p_pt,         "d_lep_p_pt/F");  
        output_tree_particle->Branch( "d_lep_p_eta",              &b_particle_lep_p_eta,        "d_lep_p_eta/F"); 
        output_tree_particle->Branch( "d_lep_p_phi",              &b_particle_lep_p_phi,        "d_lep_p_phi/F"); 
        output_tree_particle->Branch( "d_lep_p_e",                &b_particle_lep_p_e,          "d_lep_p_e/F");   
        output_tree_particle->Branch( "d_lep_p_type",             &b_particle_lep_p_type,       "d_lep_p_type/F");                
        output_tree_particle->Branch( "d_lep_m_pt",               &b_particle_lep_m_pt,         "d_lep_m_pt/F");   
        output_tree_particle->Branch( "d_lep_m_eta",              &b_particle_lep_m_eta,        "d_lep_m_eta/F");  
        output_tree_particle->Branch( "d_lep_m_phi",              &b_particle_lep_m_phi,        "d_lep_m_phi/F"); 
        output_tree_particle->Branch( "d_lep_m_e",                &b_particle_lep_m_e,          "d_lep_m_e/F");
        output_tree_particle->Branch( "d_lep_m_type",             &b_particle_lep_m_type,       "d_lep_m_type/F");        
        output_tree_particle->Branch( "d_b_pt",                   &b_particle_b_pt,             "d_b_pt/F");          
        output_tree_particle->Branch( "d_b_eta",                  &b_particle_b_eta,            "d_b_eta/F"); 
        output_tree_particle->Branch( "d_b_phi",                  &b_particle_b_phi,            "d_b_phi/F"); 
        output_tree_particle->Branch( "d_b_e",                    &b_particle_b_e,              "d_b_m/F");   
        output_tree_particle->Branch( "d_bbar_pt",                &b_particle_bbar_pt,          "d_bbar_pt/F");   
        output_tree_particle->Branch( "d_bbar_eta",               &b_particle_bbar_eta,         "d_bbar_eta/F");  
        output_tree_particle->Branch( "d_bbar_phi",               &b_particle_bbar_phi,         "d_bbar_phi/F"); 
        output_tree_particle->Branch( "d_bbar_e",                 &b_particle_bbar_e,           "d_bbar_e/F");
        output_tree_particle->Branch( "d_nu_pt",                  &b_particle_nu_pt,            "d_nu_pt/F");          
        output_tree_particle->Branch( "d_nu_eta",                 &b_particle_nu_eta,           "d_nu_eta/F"); 
        output_tree_particle->Branch( "d_nu_phi",                 &b_particle_nu_phi,           "d_nu_phi/F"); 
        output_tree_particle->Branch( "d_nu_e",                   &b_particle_nu_e,             "d_nu_m/F");   
        output_tree_particle->Branch( "d_nubar_pt",               &b_particle_nubar_pt,         "d_nubar_pt/F");   
        output_tree_particle->Branch( "d_nubar_eta",              &b_particle_nubar_eta,        "d_nubar_eta/F");  
        output_tree_particle->Branch( "d_nubar_phi",              &b_particle_nubar_phi,        "d_nubar_phi/F"); 
        output_tree_particle->Branch( "d_nubar_e",                &b_particle_nubar_e,          "d_nubar_e/F");
        output_tree_particle->Branch( "d_met_et",                 &b_particle_met_et,           "d_met_et/F");
        output_tree_particle->Branch( "d_met_phi",                &b_marticle_met_phi,          "d_met_phi/F");
        output_tree_particle->Branch( "d_met_ex",                 &b_particle_met_ex,           "d_met_ex/F");
        output_tree_particle->Branch( "d_met_ey",                 &b_particle_met_ey,           "d_met_ey/F"); 
        output_tree_particle->Branch( "d_lep_n",                  &b_particle_lep_n,            "d_lep_n/I");        
        output_tree_particle->Branch( "d_lep_pt",                 &b_particle_lep_pt);   
        output_tree_particle->Branch( "d_lep_eta",                &b_particle_lep_eta);  
        output_tree_particle->Branch( "d_lep_phi",                &b_particle_lep_phi); 
        output_tree_particle->Branch( "d_lep_e",                  &b_particle_lep_e);  
        output_tree_particle->Branch( "d_lep_charge",             &b_particle_lep_charge);
        output_tree_particle->Branch( "d_lep_type",               &b_particle_lep_type);        
        output_tree_particle->Branch( "d_jet_n",                  &b_particle_jet_n,            "d_jet_n/I");
        output_tree_particle->Branch( "d_jet_pt",                 &b_particle_jet_pt);   
        output_tree_particle->Branch( "d_jet_eta",                &b_particle_jet_eta);  
        output_tree_particle->Branch( "d_jet_phi",                &b_particle_jet_phi); 
        output_tree_particle->Branch( "d_jet_e",                  &b_particle_jet_e);  
        output_tree_particle->Branch( "d_jet_charge",             &b_particle_jet_charge);
        output_tree_particle->Branch( "d_bjet_n",                 &b_particle_bjet_n,           "d_bjet_n/I");
        output_tree_particle->Branch( "d_bjet_pt",                &b_particle_bjet_pt);   
        output_tree_particle->Branch( "d_bjet_eta",               &b_particle_bjet_eta);  
        output_tree_particle->Branch( "d_bjet_phi",               &b_particle_bjet_phi); 
        output_tree_particle->Branch( "d_bjet_e",                 &b_particle_bjet_e);  
        output_tree_particle->Branch( "d_bjet_charge",            &b_particle_bjet_charge);
        output_tree_particle->Branch( "d_nus_n",                  &b_particle_nus_n,             "d_nus_n/I");
      }
    
      void setBranchesReco(TTree* output_tree_reco){
        output_tree_reco->Branch(     "d_event_weight",           &b_reco_event_weight,      "d_event_weight/F");
        output_tree_reco->Branch(     "d_event_number",           &b_reco_event_number,      "d_event_number/F");
        output_tree_reco->Branch(     "d_atlas_top_pt",           &b_atlas_top_pt,           "d_atlas_top_pt/F");
        output_tree_reco->Branch(     "d_atlas_top_eta",          &b_atlas_top_eta,          "d_atlas_top_eta/F");
        output_tree_reco->Branch(     "d_atlas_top_phi",          &b_atlas_top_phi,          "d_atlas_top_phi/F");
        output_tree_reco->Branch(     "d_atlas_top_m",            &b_atlas_top_m,            "d_atlas_top_m/F");
        output_tree_reco->Branch(     "d_atlas_top_e",            &b_atlas_top_e,            "d_atlas_top_e/F");
        output_tree_reco->Branch(     "d_atlas_top_y",            &b_atlas_top_y,            "d_atlas_top_y/F");
        output_tree_reco->Branch(     "d_atlas_tbar_pt",          &b_atlas_tbar_pt,          "d_atlas_tbar_pt/F");
        output_tree_reco->Branch(     "d_atlas_tbar_eta",         &b_atlas_tbar_eta,         "d_atlas_tbar_eta/F");
        output_tree_reco->Branch(     "d_atlas_tbar_phi",         &b_atlas_tbar_phi,         "d_atlas_tbar_phi/F");
        output_tree_reco->Branch(     "d_atlas_tbar_m",           &b_atlas_tbar_m,           "d_atlas_tbar_m/F");
        output_tree_reco->Branch(     "d_atlas_tbar_e",           &b_atlas_tbar_e,           "d_atlas_tbar_e/F");
        output_tree_reco->Branch(     "d_atlas_tbar_y",           &b_atlas_tbar_y,           "d_atlas_tbar_y/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_pt",         &b_atlas_ttbar_pt,         "d_atlas_ttbar_pt/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_eta",        &b_atlas_ttbar_eta,        "d_atlas_ttbar_eta/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_phi",        &b_atlas_ttbar_phi,        "d_atlas_ttbar_phi/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_m",          &b_atlas_ttbar_m,          "d_atlas_ttbar_m/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_e",          &b_atlas_ttbar_e,          "d_atlas_ttbar_e/F");
        output_tree_reco->Branch(     "d_atlas_ttbar_y",          &b_atlas_ttbar_y,          "d_atlas_ttbar_u/F");
        output_tree_reco->Branch(     "d_atlas_cos_helicity_p",   &b_atlas_cos_helicity_p,   "d_atlas_cos_helicity_p/F");
        output_tree_reco->Branch(     "d_atlas_cos_helicity_m",   &b_atlas_cos_helicity_m,   "d_atlas_cos_helicity_m/F");
        output_tree_reco->Branch(     "d_atlas_cos_transverse_p", &b_atlas_cos_transverse_p, "d_atlas_cos_transverse_p/F");        
        output_tree_reco->Branch(     "d_atlas_cos_transverse_m", &b_atlas_cos_transverse_m, "d_atlas_cos_transverse_m/F");               
        output_tree_reco->Branch(     "d_atlas_cos_raxis_p",      &b_atlas_cos_raxis_p,      "d_atlas_cos_raxis_p/F");
        output_tree_reco->Branch(     "d_atlas_cos_raxis_m",      &b_atlas_cos_raxis_m,      "d_atlas_cos_raxis_m/F");
        output_tree_reco->Branch(     "d_atlas_boson_pt",         &b_atlas_boson_pt,         "d_atlas_boson_pt/F");
        output_tree_reco->Branch(     "d_atlas_boson_eta",        &b_atlas_boson_eta,        "d_atlas_boson_eta/F");
        output_tree_reco->Branch(     "d_atlas_boson_phi",        &b_atlas_boson_phi,        "d_atlas_boson_phi/F");
        output_tree_reco->Branch(     "d_atlas_boson_m",          &b_atlas_boson_m,          "d_atlas_boson_m/F");
        output_tree_reco->Branch(     "d_atlas_boson_e",          &b_atlas_boson_e,          "d_atlas_boson_e/F");
        output_tree_reco->Branch(     "d_atlas_boson_y",          &b_atlas_boson_y,          "d_atlas_boson_y/F");
        output_tree_reco->Branch(     "d_atlas_lep_p_pt",         &b_atlas_lep_p_pt,         "d_atlas_lep_p_pt/F");  
        output_tree_reco->Branch(     "d_atlas_lep_p_eta",        &b_atlas_lep_p_eta,        "d_atlas_lep_p_eta/F"); 
        output_tree_reco->Branch(     "d_atlas_lep_p_phi",        &b_atlas_lep_p_phi,        "d_atlas_lep_p_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_lep_p_e",          &b_atlas_lep_p_e,          "d_atlas_lep_p_e/F");   
        output_tree_reco->Branch(     "d_atlas_lep_p_type",       &b_atlas_lep_p_type,       "d_atlas_lep_p_type/F");                
        output_tree_reco->Branch(     "d_atlas_lep_m_pt",         &b_atlas_lep_m_pt,         "d_atlas_lep_m_pt/F");   
        output_tree_reco->Branch(     "d_atlas_lep_m_eta",        &b_atlas_lep_m_eta,        "d_atlas_lep_m_eta/F");  
        output_tree_reco->Branch(     "d_atlas_lep_m_phi",        &b_atlas_lep_m_phi,        "d_atlas_lep_m_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_lep_m_e",          &b_atlas_lep_m_e,          "d_atlas_lep_m_e/F");
        output_tree_reco->Branch(     "d_atlas_lep_m_type",       &b_atlas_lep_m_type,       "d_atlas_lep_m_type/F");        
        output_tree_reco->Branch(     "d_atlas_b_pt",             &b_atlas_b_pt,             "d_atlas_b_pt/F");          
        output_tree_reco->Branch(     "d_atlas_b_eta",            &b_atlas_b_eta,            "d_atlas_b_eta/F"); 
        output_tree_reco->Branch(     "d_atlas_b_phi",            &b_atlas_b_phi,            "d_atlas_b_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_b_e",              &b_atlas_b_e,              "d_atlas_b_m/F");   
        output_tree_reco->Branch(     "d_atlas_bbar_pt",          &b_atlas_bbar_pt,          "d_atlas_bbar_pt/F");   
        output_tree_reco->Branch(     "d_atlas_bbar_eta",         &b_atlas_bbar_eta,         "d_atlas_bbar_eta/F");  
        output_tree_reco->Branch(     "d_atlas_bbar_phi",         &b_atlas_bbar_phi,         "d_atlas_bbar_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_bbar_e",           &b_atlas_bbar_e,           "d_atlas_bbar_e/F");
        output_tree_reco->Branch(     "d_atlas_nu_pt",            &b_atlas_nu_pt,            "d_atlas_nu_pt/F");          
        output_tree_reco->Branch(     "d_atlas_nu_eta",           &b_atlas_nu_eta,           "d_atlas_nu_eta/F"); 
        output_tree_reco->Branch(     "d_atlas_nu_phi",           &b_atlas_nu_phi,           "d_atlas_nu_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_nu_e",             &b_atlas_nu_e,             "d_atlas_nu_m/F");   
        output_tree_reco->Branch(     "d_atlas_nubar_pt",         &b_atlas_nubar_pt,         "d_atlas_nubar_pt/F");   
        output_tree_reco->Branch(     "d_atlas_nubar_eta",        &b_atlas_nubar_eta,        "d_atlas_nubar_eta/F");  
        output_tree_reco->Branch(     "d_atlas_nubar_phi",        &b_atlas_nubar_phi,        "d_atlas_nubar_phi/F"); 
        output_tree_reco->Branch(     "d_atlas_nubar_e",          &b_atlas_nubar_e,          "d_atlas_nubar_e/F");
        output_tree_reco->Branch(     "d_atlas_met_et",           &b_atlas_met_et,           "d_atlas_met_et/F");
        output_tree_reco->Branch(     "d_atlas_met_phi",          &b_atlas_met_phi,          "d_atlas_met_phi/F");
        output_tree_reco->Branch(     "d_atlas_met_ex",           &b_atlas_met_ex,           "d_atlas_met_ex/F");
        output_tree_reco->Branch(     "d_atlas_met_ey",           &b_atlas_met_ey,           "d_atlas_met_ey/F"); 
        output_tree_reco->Branch(     "d_atlas_lep_n",            &b_atlas_lep_n,            "d_atlas_lep_n/I");        
        output_tree_reco->Branch(     "d_atlas_lep_pt",           &b_atlas_lep_pt);   
        output_tree_reco->Branch(     "d_atlas_lep_eta",          &b_atlas_lep_eta);  
        output_tree_reco->Branch(     "d_atlas_lep_phi",          &b_atlas_lep_phi); 
        output_tree_reco->Branch(     "d_atlas_lep_e",            &b_atlas_lep_e);  
        output_tree_reco->Branch(     "d_atlas_lep_charge",       &b_atlas_lep_charge);
        output_tree_reco->Branch(     "d_atlas_lep_type",         &b_atlas_lep_type);        
        output_tree_reco->Branch(     "d_atlas_jet_n",            &b_atlas_jet_n,            "d_atlas_bjet_n/I");
        output_tree_reco->Branch(     "d_atlas_jet_pt",           &b_atlas_jet_pt);   
        output_tree_reco->Branch(     "d_atlas_jet_eta",          &b_atlas_jet_eta);  
        output_tree_reco->Branch(     "d_atlas_jet_phi",          &b_atlas_jet_phi); 
        output_tree_reco->Branch(     "d_atlas_jet_e",            &b_atlas_jet_e);  
        output_tree_reco->Branch(     "d_atlas_bjet_n",           &b_atlas_bjet_n,           "d_atlas_bjet_n/I");
        output_tree_reco->Branch(     "d_atlas_bjet_pt",          &b_atlas_bjet_pt);   
        output_tree_reco->Branch(     "d_atlas_bjet_eta",         &b_atlas_bjet_eta);  
        output_tree_reco->Branch(     "d_atlas_bjet_phi",         &b_atlas_bjet_phi); 
        output_tree_reco->Branch(     "d_atlas_bjet_e",           &b_atlas_bjet_e); 

        output_tree_reco->Branch(     "d_cms_top_pt",             &b_cms_top_pt,           "d_cms_top_pt/F");
        output_tree_reco->Branch(     "d_cms_top_eta",            &b_cms_top_eta,          "d_cms_top_eta/F");
        output_tree_reco->Branch(     "d_cms_top_phi",            &b_cms_top_phi,          "d_cms_top_phi/F");
        output_tree_reco->Branch(     "d_cms_top_m",              &b_cms_top_m,            "d_cms_top_m/F");
        output_tree_reco->Branch(     "d_cms_top_e",              &b_cms_top_e,            "d_cms_top_e/F");
        output_tree_reco->Branch(     "d_cms_top_y",              &b_cms_top_y,            "d_cms_top_y/F");
        output_tree_reco->Branch(     "d_cms_tbar_pt",            &b_cms_tbar_pt,          "d_cms_tbar_pt/F");
        output_tree_reco->Branch(     "d_cms_tbar_eta",           &b_cms_tbar_eta,         "d_cms_tbar_eta/F");
        output_tree_reco->Branch(     "d_cms_tbar_phi",           &b_cms_tbar_phi,         "d_cms_tbar_phi/F");
        output_tree_reco->Branch(     "d_cms_tbar_m",             &b_cms_tbar_m,           "d_cms_tbar_m/F");
        output_tree_reco->Branch(     "d_cms_tbar_e",             &b_cms_tbar_e,           "d_cms_tbar_e/F");
        output_tree_reco->Branch(     "d_cms_tbar_y",             &b_cms_tbar_y,           "d_cms_tbar_y/F");
        output_tree_reco->Branch(     "d_cms_ttbar_pt",           &b_cms_ttbar_pt,         "d_cms_ttbar_pt/F");
        output_tree_reco->Branch(     "d_cms_ttbar_eta",          &b_cms_ttbar_eta,        "d_cms_ttbar_eta/F");
        output_tree_reco->Branch(     "d_cms_ttbar_phi",          &b_cms_ttbar_phi,        "d_cms_ttbar_phi/F");
        output_tree_reco->Branch(     "d_cms_ttbar_m",            &b_cms_ttbar_m,          "d_cms_ttbar_m/F");
        output_tree_reco->Branch(     "d_cms_ttbar_e",            &b_cms_ttbar_e,          "d_cms_ttbar_e/F");
        output_tree_reco->Branch(     "d_cms_ttbar_y",            &b_cms_ttbar_y,          "d_cms_ttbar_u/F");
        output_tree_reco->Branch(     "d_cms_cos_helicity_p",     &b_cms_cos_helicity_p,   "d_cms_cos_helicity_p/F");
        output_tree_reco->Branch(     "d_cms_cos_helicity_m",     &b_cms_cos_helicity_m,   "d_cms_cos_helicity_m/F");
        output_tree_reco->Branch(     "d_cms_cos_transverse_p",   &b_cms_cos_transverse_p, "d_cms_cos_transverse_p/F");        
        output_tree_reco->Branch(     "d_cms_cos_transverse_m",   &b_cms_cos_transverse_m, "d_cms_cos_transverse_m/F");               
        output_tree_reco->Branch(     "d_cms_cos_raxis_p",        &b_cms_cos_raxis_p,      "d_cms_cos_raxis_p/F");
        output_tree_reco->Branch(     "d_cms_cos_raxis_m",        &b_cms_cos_raxis_m,      "d_cms_cos_raxis_m/F");
        output_tree_reco->Branch(     "d_cms_boson_pt",           &b_cms_boson_pt,         "d_cms_boson_pt/F");
        output_tree_reco->Branch(     "d_cms_boson_eta",          &b_cms_boson_eta,        "d_cms_boson_eta/F");
        output_tree_reco->Branch(     "d_cms_boson_phi",          &b_cms_boson_phi,        "d_cms_boson_phi/F");
        output_tree_reco->Branch(     "d_cms_boson_m",            &b_cms_boson_m,          "d_cms_boson_m/F");
        output_tree_reco->Branch(     "d_cms_boson_e",            &b_cms_boson_e,          "d_cms_boson_e/F");
        output_tree_reco->Branch(     "d_cms_boson_y",            &b_cms_boson_y,          "d_cms_boson_y/F");
        output_tree_reco->Branch(     "d_cms_lep_p_pt",           &b_cms_lep_p_pt,         "d_cms_lep_p_pt/F");  
        output_tree_reco->Branch(     "d_cms_lep_p_eta",          &b_cms_lep_p_eta,        "d_cms_lep_p_eta/F"); 
        output_tree_reco->Branch(     "d_cms_lep_p_phi",          &b_cms_lep_p_phi,        "d_cms_lep_p_phi/F"); 
        output_tree_reco->Branch(     "d_cms_lep_p_e",            &b_cms_lep_p_e,          "d_cms_lep_p_e/F");   
        output_tree_reco->Branch(     "d_cms_lep_p_type",         &b_cms_lep_p_type,       "d_cms_lep_p_type/F");                
        output_tree_reco->Branch(     "d_cms_lep_m_pt",           &b_cms_lep_m_pt,         "d_cms_lep_m_pt/F");   
        output_tree_reco->Branch(     "d_cms_lep_m_eta",          &b_cms_lep_m_eta,        "d_cms_lep_m_eta/F");  
        output_tree_reco->Branch(     "d_cms_lep_m_phi",          &b_cms_lep_m_phi,        "d_cms_lep_m_phi/F"); 
        output_tree_reco->Branch(     "d_cms_lep_m_e",            &b_cms_lep_m_e,          "d_cms_lep_m_e/F");
        output_tree_reco->Branch(     "d_cms_lep_m_type",         &b_cms_lep_m_type,       "d_cms_lep_m_type/F");        
        output_tree_reco->Branch(     "d_cms_b_pt",               &b_cms_b_pt,             "d_cms_b_pt/F");          
        output_tree_reco->Branch(     "d_cms_b_eta",              &b_cms_b_eta,            "d_cms_b_eta/F"); 
        output_tree_reco->Branch(     "d_cms_b_phi",              &b_cms_b_phi,            "d_cms_b_phi/F"); 
        output_tree_reco->Branch(     "d_cms_b_e",                &b_cms_b_e,              "d_cms_b_m/F");   
        output_tree_reco->Branch(     "d_cms_bbar_pt",            &b_cms_bbar_pt,          "d_cms_bbar_pt/F");   
        output_tree_reco->Branch(     "d_cms_bbar_eta",           &b_cms_bbar_eta,         "d_cms_bbar_eta/F");  
        output_tree_reco->Branch(     "d_cms_bbar_phi",           &b_cms_bbar_phi,         "d_cms_bbar_phi/F"); 
        output_tree_reco->Branch(     "d_cms_bbar_e",             &b_cms_bbar_e,           "d_cms_bbar_e/F");
        output_tree_reco->Branch(     "d_cms_nu_pt",              &b_cms_nu_pt,            "d_cms_nu_pt/F");          
        output_tree_reco->Branch(     "d_cms_nu_eta",             &b_cms_nu_eta,           "d_cms_nu_eta/F"); 
        output_tree_reco->Branch(     "d_cms_nu_phi",             &b_cms_nu_phi,           "d_cms_nu_phi/F"); 
        output_tree_reco->Branch(     "d_cms_nu_e",               &b_cms_nu_e,             "d_cms_nu_m/F");   
        output_tree_reco->Branch(     "d_cms_nubar_pt",           &b_cms_nubar_pt,         "d_cms_nubar_pt/F");   
        output_tree_reco->Branch(     "d_cms_nubar_eta",          &b_cms_nubar_eta,        "d_cms_nubar_eta/F");  
        output_tree_reco->Branch(     "d_cms_nubar_phi",          &b_cms_nubar_phi,        "d_cms_nubar_phi/F"); 
        output_tree_reco->Branch(     "d_cms_nubar_e",            &b_cms_nubar_e,          "d_cms_nubar_e/F");
        output_tree_reco->Branch(     "d_cms_met_et",             &b_cms_met_et,           "d_cms_met_et/F");
        output_tree_reco->Branch(     "d_cms_met_phi",            &b_cms_met_phi,          "d_cms_met_phi/F");
        output_tree_reco->Branch(     "d_cms_met_ex",             &b_cms_met_ex,           "d_cms_met_ex/F");
        output_tree_reco->Branch(     "d_cms_met_ey",             &b_cms_met_ey,           "d_cms_met_ey/F"); 
        output_tree_reco->Branch(     "d_cms_lep_n",              &b_cms_lep_n,            "d_cms_lep_n/I");        
        output_tree_reco->Branch(     "d_cms_lep_pt",             &b_cms_lep_pt);   
        output_tree_reco->Branch(     "d_cms_lep_eta",            &b_cms_lep_eta);  
        output_tree_reco->Branch(     "d_cms_lep_phi",            &b_cms_lep_phi); 
        output_tree_reco->Branch(     "d_cms_lep_e",              &b_cms_lep_e);  
        output_tree_reco->Branch(     "d_cms_lep_charge",         &b_cms_lep_charge);
        output_tree_reco->Branch(     "d_cms_lep_type",           &b_cms_lep_type);        
        output_tree_reco->Branch(     "d_cms_jet_n",              &b_cms_jet_n,            "d_atlas_bjet_n/I");
        output_tree_reco->Branch(     "d_cms_jet_pt",             &b_cms_jet_pt);   
        output_tree_reco->Branch(     "d_cms_jet_eta",            &b_cms_jet_eta);  
        output_tree_reco->Branch(     "d_cms_jet_phi",            &b_cms_jet_phi); 
        output_tree_reco->Branch(     "d_cms_jet_e",              &b_cms_jet_e);  
        output_tree_reco->Branch(     "d_cms_bjet_n",             &b_cms_bjet_n,           "d_atlas_bjet_n/I");
        output_tree_reco->Branch(     "d_cms_bjet_pt",            &b_cms_bjet_pt);   
        output_tree_reco->Branch(     "d_cms_bjet_eta",           &b_cms_bjet_eta);  
        output_tree_reco->Branch(     "d_cms_bjet_phi",           &b_cms_bjet_phi); 
        output_tree_reco->Branch(     "d_cms_bjet_e",             &b_cms_bjet_e);
      }

      void ResetBranches(){

        b_parton_event_weight       = -99.;
        b_parton_event_number       = -99.;
        b_parton_top_pt             = -99.;
        b_parton_top_eta            = -99.;
        b_parton_top_phi            = -99.;
        b_parton_top_m              = -99.;
        b_parton_top_e              = -99.;
        b_parton_top_y              = -99.;
        b_parton_tbar_pt            = -99.;
        b_parton_tbar_eta           = -99.;
        b_parton_tbar_phi           = -99.;
        b_parton_tbar_m             = -99.;
        b_parton_tbar_e             = -99.;
        b_parton_tbar_y             = -99.;
        b_parton_ttbar_pt           = -99.;
        b_parton_ttbar_eta          = -99.;
        b_parton_ttbar_phi          = -99.;
        b_parton_ttbar_m            = -99.;
        b_parton_ttbar_e            = -99.;
        b_parton_ttbar_y            = -99.;
        b_parton_cos_helicity_p     = -99.;
        b_parton_cos_helicity_m     = -99.;
        b_parton_cos_transverse_p   = -99.;
        b_parton_cos_transverse_m   = -99.;
        b_parton_cos_raxis_p        = -99.;
        b_parton_cos_raxis_m        = -99.;
        b_parton_boson_pt           = -99.;
        b_parton_boson_eta          = -99.;
        b_parton_boson_phi          = -99.;
        b_parton_boson_m            = -99.;
        b_parton_boson_e            = -99.;
        b_parton_boson_y            = -99.;
        b_parton_lep_p_pt           = -99.;  
        b_parton_lep_p_eta          = -99.; 
        b_parton_lep_p_phi          = -99.; 
        b_parton_lep_p_e            = -99.;   
        b_parton_lep_p_type         = -99.;           
        b_parton_lep_m_pt           = -99.;   
        b_parton_lep_m_eta          = -99.;  
        b_parton_lep_m_phi          = -99.; 
        b_parton_lep_m_e            = -99.;
        b_parton_lep_m_type         = -99.;        
        b_parton_b_pt               = -99.;          
        b_parton_b_eta              = -99.; 
        b_parton_b_phi              = -99.; 
        b_parton_b_e                = -99.;   
        b_parton_bbar_pt            = -99.;   
        b_parton_bbar_eta           = -99.;  
        b_parton_bbar_phi           = -99.;
        b_parton_nu_pt              = -99.;          
        b_parton_nu_eta             = -99.; 
        b_parton_nu_phi             = -99.; 
        b_parton_nu_e               = -99.;   
        b_parton_nubar_pt           = -99.;   
        b_parton_nubar_eta          = -99.;  
        b_parton_nubar_phi          = -99.; 
        b_parton_nubar_e            = -99.;
        b_parton_met_et             = -99.;
        b_parton_met_phi            = -99.;
        b_parton_met_ex             = -99.;
        b_parton_met_ey             = -99.; 
        b_particle_event_weight     = -99.;
        b_particle_event_number     = -99.;
        b_particle_top_pt           = -99.;
        b_particle_top_eta          = -99.;
        b_particle_top_phi          = -99.;
        b_particle_top_m            = -99.;
        b_particle_top_e            = -99.;
        b_particle_top_y            = -99.;                                        
        b_particle_tbar_pt          = -99.;
        b_particle_tbar_eta         = -99.;
        b_particle_tbar_phi         = -99.;
        b_particle_tbar_m           = -99.;
        b_particle_tbar_e           = -99.;
        b_particle_tbar_y           = -99.; 
        b_particle_ttbar_pt         = -99.;
        b_particle_ttbar_eta        = -99.;
        b_particle_ttbar_phi        = -99.;
        b_particle_ttbar_m          = -99.;
        b_particle_ttbar_e          = -99.;
        b_particle_ttbar_y          = -99.; 
        b_particle_cos_helicity_p   = -99.;
        b_particle_cos_helicity_m   = -99.;
        b_particle_cos_transverse_p = -99.;
        b_particle_cos_transverse_m = -99.;
        b_particle_cos_raxis_p      = -99.;
        b_particle_cos_raxis_m      = -99.;
        b_particle_boson_pt         = -99.;
        b_particle_boson_eta        = -99.;
        b_particle_boson_phi        = -99.;
        b_particle_boson_m          = -99.;
        b_particle_boson_e          = -99.;
        b_particle_boson_y          = -99.;
        b_particle_lep_p_pt         = -99.;  
        b_particle_lep_p_eta        = -99.; 
        b_particle_lep_p_phi        = -99.; 
        b_particle_lep_p_e          = -99.;   
        b_particle_lep_p_type       = -99.;           
        b_particle_lep_m_pt         = -99.;   
        b_particle_lep_m_eta        = -99.;  
        b_particle_lep_m_phi        = -99.; 
        b_particle_lep_m_e          = -99.;
        b_particle_lep_m_type       = -99.;        
        b_particle_b_pt             = -99.;          
        b_particle_b_eta            = -99.; 
        b_particle_b_phi            = -99.; 
        b_particle_b_e              = -99.;   
        b_particle_bbar_pt          = -99.;   
        b_particle_bbar_eta         = -99.;  
        b_particle_bbar_phi         = -99.; 
        b_particle_bbar_e           = -99.;
        b_particle_nu_pt            = -99.;          
        b_particle_nu_eta           = -99.; 
        b_particle_nu_phi           = -99.; 
        b_particle_nu_e             = -99.;   
        b_particle_nubar_pt         = -99.;   
        b_particle_nubar_eta        = -99.;  
        b_particle_nubar_phi        = -99.; 
        b_particle_nubar_e          = -99.;
        b_particle_met_et           = -99.;
        b_marticle_met_phi          = -99.;
        b_particle_met_ex           = -99.;
        b_particle_met_ey           = -99.;
        b_particle_lep_n            = -1.;
        b_particle_lep_pt.clear();   
        b_particle_lep_eta.clear();  
        b_particle_lep_phi.clear(); 
        b_particle_lep_e.clear();  
        b_particle_lep_charge.clear();
        b_particle_lep_type.clear();        
        b_particle_jet_n            = -1;
        b_particle_jet_pt.clear();   
        b_particle_jet_eta.clear();  
        b_particle_jet_phi.clear(); 
        b_particle_jet_e.clear();  
        b_particle_jet_charge.clear();
        b_particle_bjet_n           = -1;
        b_particle_bjet_pt.clear();   
        b_particle_bjet_eta.clear();  
        b_particle_bjet_phi.clear(); 
        b_particle_bjet_e.clear();  
        b_particle_bjet_charge.clear();
        b_particle_nus_n            = -1;

        b_reco_event_weight      = -99.;
        b_reco_event_number      = -99.;

        b_atlas_top_pt           = -99.;
        b_atlas_top_eta          = -99.;
        b_atlas_top_phi          = -99.;
        b_atlas_top_m            = -99.;
        b_atlas_top_e            = -99.;
        b_atlas_top_y            = -99.;                                        
        b_atlas_tbar_pt          = -99.;
        b_atlas_tbar_eta         = -99.;
        b_atlas_tbar_phi         = -99.;
        b_atlas_tbar_m           = -99.;
        b_atlas_tbar_e           = -99.;
        b_atlas_tbar_y           = -99.; 
        b_atlas_ttbar_pt         = -99.;
        b_atlas_ttbar_eta        = -99.;
        b_atlas_ttbar_phi        = -99.;
        b_atlas_ttbar_m          = -99.;
        b_atlas_ttbar_e          = -99.;
        b_atlas_ttbar_y          = -99.; 
        b_atlas_cos_helicity_p   = -99.;
        b_atlas_cos_helicity_m   = -99.;
        b_atlas_cos_transverse_p = -99.;
        b_atlas_cos_transverse_m = -99.;
        b_atlas_cos_raxis_p      = -99.;
        b_atlas_cos_raxis_m      = -99.;
        b_atlas_boson_pt         = -99.;
        b_atlas_boson_eta        = -99.;
        b_atlas_boson_phi        = -99.;
        b_atlas_boson_m          = -99.;
        b_atlas_boson_e          = -99.;
        b_atlas_boson_y          = -99.;
        b_atlas_lep_p_pt         = -99.;  
        b_atlas_lep_p_eta        = -99.; 
        b_atlas_lep_p_phi        = -99.; 
        b_atlas_lep_p_e          = -99.;   
        b_atlas_lep_p_type       = -99.;           
        b_atlas_lep_m_pt         = -99.;   
        b_atlas_lep_m_eta        = -99.;  
        b_atlas_lep_m_phi        = -99.; 
        b_atlas_lep_m_e          = -99.;
        b_atlas_lep_m_type       = -99.;        
        b_atlas_b_pt             = -99.;          
        b_atlas_b_eta            = -99.; 
        b_atlas_b_phi            = -99.; 
        b_atlas_b_e              = -99.;   
        b_atlas_bbar_pt          = -99.;   
        b_atlas_bbar_eta         = -99.;  
        b_atlas_bbar_phi         = -99.; 
        b_atlas_bbar_e           = -99.;
        b_atlas_nu_pt            = -99.;          
        b_atlas_nu_eta           = -99.; 
        b_atlas_nu_phi           = -99.; 
        b_atlas_nu_e             = -99.;   
        b_atlas_nubar_pt         = -99.;   
        b_atlas_nubar_eta        = -99.;  
        b_atlas_nubar_phi        = -99.; 
        b_atlas_nubar_e          = -99.;
        b_atlas_met_et           = -99.;
        b_atlas_met_phi          = -99.;
        b_atlas_met_ex           = -99.;
        b_atlas_met_ey           = -99.;
        b_atlas_lep_n            = -1.;
        b_atlas_lep_pt.clear();   
        b_atlas_lep_eta.clear();  
        b_atlas_lep_phi.clear(); 
        b_atlas_lep_e.clear();  
        b_atlas_lep_charge.clear();
        b_atlas_lep_type.clear();        
        b_atlas_jet_n            = -1;
        b_atlas_jet_pt.clear();   
        b_atlas_jet_eta.clear();  
        b_atlas_jet_phi.clear(); 
        b_atlas_jet_e.clear();  
        b_atlas_bjet_n           = -1;
        b_atlas_bjet_pt.clear();   
        b_atlas_bjet_eta.clear();  
        b_atlas_bjet_phi.clear(); 
        b_atlas_bjet_e.clear();  

        b_cms_top_pt           = -99.;
        b_cms_top_eta          = -99.;
        b_cms_top_phi          = -99.;
        b_cms_top_m            = -99.;
        b_cms_top_e            = -99.;
        b_cms_top_y            = -99.;                                        
        b_cms_tbar_pt          = -99.;
        b_cms_tbar_eta         = -99.;
        b_cms_tbar_phi         = -99.;
        b_cms_tbar_m           = -99.;
        b_cms_tbar_e           = -99.;
        b_cms_tbar_y           = -99.; 
        b_cms_ttbar_pt         = -99.;
        b_cms_ttbar_eta        = -99.;
        b_cms_ttbar_phi        = -99.;
        b_cms_ttbar_m          = -99.;
        b_cms_ttbar_e          = -99.;
        b_cms_ttbar_y          = -99.; 
        b_cms_cos_helicity_p   = -99.;
        b_cms_cos_helicity_m   = -99.;
        b_cms_cos_transverse_p = -99.;
        b_cms_cos_transverse_m = -99.;
        b_cms_cos_raxis_p      = -99.;
        b_cms_cos_raxis_m      = -99.;
        b_cms_boson_pt         = -99.;
        b_cms_boson_eta        = -99.;
        b_cms_boson_phi        = -99.;
        b_cms_boson_m          = -99.;
        b_cms_boson_e          = -99.;
        b_cms_boson_y          = -99.;
        b_cms_lep_p_pt         = -99.;  
        b_cms_lep_p_eta        = -99.; 
        b_cms_lep_p_phi        = -99.; 
        b_cms_lep_p_e          = -99.;   
        b_cms_lep_p_type       = -99.;           
        b_cms_lep_m_pt         = -99.;   
        b_cms_lep_m_eta        = -99.;  
        b_cms_lep_m_phi        = -99.; 
        b_cms_lep_m_e          = -99.;
        b_cms_lep_m_type       = -99.;        
        b_cms_b_pt             = -99.;          
        b_cms_b_eta            = -99.; 
        b_cms_b_phi            = -99.; 
        b_cms_b_e              = -99.;   
        b_cms_bbar_pt          = -99.;   
        b_cms_bbar_eta         = -99.;  
        b_cms_bbar_phi         = -99.; 
        b_cms_bbar_e           = -99.;
        b_cms_nu_pt            = -99.;          
        b_cms_nu_eta           = -99.; 
        b_cms_nu_phi           = -99.; 
        b_cms_nu_e             = -99.;   
        b_cms_nubar_pt         = -99.;   
        b_cms_nubar_eta        = -99.;  
        b_cms_nubar_phi        = -99.; 
        b_cms_nubar_e          = -99.;
        b_cms_met_et           = -99.;
        b_cms_met_phi          = -99.;
        b_cms_met_ex           = -99.;
        b_cms_met_ey           = -99.;
        b_cms_lep_n            = -1.;
        b_cms_lep_pt.clear();   
        b_cms_lep_eta.clear();  
        b_cms_lep_phi.clear(); 
        b_cms_lep_e.clear();  
        b_cms_lep_charge.clear();
        b_cms_lep_type.clear();        
        b_cms_jet_n            = -1;
        b_cms_jet_pt.clear();   
        b_cms_jet_eta.clear();  
        b_cms_jet_phi.clear(); 
        b_cms_jet_e.clear();  
        b_cms_bjet_n           = -1;
        b_cms_bjet_pt.clear();   
        b_cms_bjet_eta.clear();  
        b_cms_bjet_phi.clear(); 
        b_cms_bjet_e.clear();  
      }
    };// End of class definition

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(PHENO_ttX);


} // End of RIVET namespace 
